import base64
import hashlib
import logging
import random
import string
import urllib
import urlparse

import requests
from suds.client import Client
from suds.sudsobject import asdict
from suds import WebFault

from django.utils.http import urlencode
from django.conf import settings

logger = logging.getLogger(__name__)

WSDL = settings.DOSESPOT_API_URL + '?wsdl'

def get_random_unicode(length):
    alphabet = string.ascii_letters + string.digits
    return ''.join(random.choice(alphabet) for i in range(length))

def generate_sso_code(url_encode=False):
    """Generates a DoseSpot single sign on code.
    
    Returns a tuple consisting of the 32-character random unicode phrase used in the code generation, and the single sign on code.
    """
    phrase = get_random_unicode(32)
    phrase_key = ''.join([phrase, settings.DOSESPOT_CLINIC_KEY])
    phrase_key_bytes = phrase_key.encode('utf-8')
    sha512 = hashlib.sha512()
    sha512.update(phrase_key_bytes)
    digest = sha512.digest()
    b64_digest = base64.b64encode(digest)
    
    if b64_digest[-2:] == '==':
        b64_digest = b64_digest[:-2]
    
    phrase_b64_digest = ''.join([phrase, b64_digest])
    
    logger.info(phrase_b64_digest)
    
    if url_encode:
        return phrase, urllib.quote_plus(phrase_b64_digest)
    else:
        return phrase, phrase_b64_digest
    
def generate_sso_userid_verify(sso_phrase, clinician_id, url_encode=False):
    """Generates a DoseSpot single sign on user verification code.
    
    sso_phrase - the 32-character random unicode phrase used to generate the corresponding DoseSpot single sign on code, from generate_sso_code
    clinician_id - DoseSpot clinician id (user id)
    """
    phrase = sso_phrase[:22]
    id_phrase_key = ''.join([clinician_id, phrase, settings.DOSESPOT_CLINIC_KEY])
    id_phrase_key_bytes = id_phrase_key.encode('utf-8')
    sha512 = hashlib.sha512()
    sha512.update(id_phrase_key_bytes)
    digest = sha512.digest()
    b64_digest = base64.b64encode(digest)
    
    if b64_digest[-2:] == '==':
        b64_digest = b64_digest[:-2]
    
    logger.info(b64_digest)
    
    if url_encode:
        return urllib.quote_plus(b64_digest)
    else:
        return b64_digest
        
def create_dosespot_url(params):
    scheme, netloc, path, _, _, _ = urlparse.urlparse(settings.DOSESPOT_SSO_URL)
    
    url_generator = (
                scheme,
                netloc,
                path,
                '',
                urllib.urlencode(params),
                '',
    )
    url = urlparse.urlunparse(url_generator)    

    return url

def get_provider_page(clinician_id):
    (phrase, encphrase) = generate_sso_code()
    encverify = generate_sso_userid_verify(phrase, clinician_id)

    params = {
        'b': 2,
        'SingleSignOnClinicId': settings.DOSESPOT_CLINIC_ID,
        'SingleSignOnUserId': clinician_id,
        'SingleSignOnPhraseLength': 32,
        'SingleSignOnCode': encphrase,
        'SingleSignOnUserIdVerify': encverify,
        'RefillsErrors': 1,
    }
    
    url = create_dosespot_url(params)

    return url

def get_patient_redirect(patient, clinician_id):
    """Gets the DoseSpot redirect after doing an SSO login.

    patient - a PatientProfile instance for the patient getting scripts
    """
        
    (phrase, encphrase) = generate_sso_code()
    encverify = generate_sso_userid_verify(phrase, clinician_id)

    params = {
        'b': 2,
        'SingleSignOnClinicId': settings.DOSESPOT_CLINIC_ID,
        'SingleSignOnUserId': clinician_id, 
        'SingleSignOnPhraseLength': 32,
        'SingleSignOnCode': encphrase,
        'SingleSignOnUserIdVerify': encverify,
        'Prefix': '',
        'FirstName': patient.first_name,
        'MiddleName': patient.middle_name,
        'LastName': patient.last_name,
        'Suffix': '',
        'DateOfBirth': patient.birth_date.strftime("%m/%d/%Y"),
        'Gender': 'Male' if patient.gender == 'M' else 'Female',
        'MRN': '',
        'Address1': patient.mail_one,
        'Address2': patient.mail_two,
        'City': patient.city,
        'State': patient.state,
        'ZipCode': patient.zipcode,
        'PrimaryPhone': patient.phone,
        'PrimaryPhoneType': 'Cell',
        'PhoneAdditional1': '',
        'PhoneAdditionalType1': '',
        'PhoneAdditional2': '',
        'PhoneAdditionalType2': '',
        'PharmacyId': '',
    }

    if patient.dosespot_id:
        params['PatientID'] = patient.dosespot_id
        url = create_dosespot_url(params)
    else:
        r = requests.post(settings.DOSESPOT_SSO_URL,
                params=params,
                allow_redirects=False)

        if r.status_code != 302:
            raise DoseSpotException('Error authenticating with DoseSpot')
        
        redirect = r.headers['location']
        patient_id = redirect.split('?')[1].split('=')[1]
        patient.dosespot_id = patient_id
        patient.save()
            
        url = r.url

    return url

def create_sso_soap_object(clinician_id):
    (phrase, encphrase) = generate_sso_code()
    encverify = generate_sso_userid_verify(phrase, clinician_id)
    client = Client(url=WSDL, location=settings.DOSESPOT_API_URL)
    
    sso = client.factory.create('SingleSignOn')
    sso.SingleSignOnClinicId = settings.DOSESPOT_CLINIC_ID
    sso.SingleSignOnCode = encphrase
    sso.SingleSignOnUserId = clinician_id
    sso.SingleSignOnUserIdVerify = encverify
    sso.SingleSignOnPhraseLength = 32    
    
    return sso

def get_transmission_errors(clinician_id):
    client = Client(url=WSDL, location=settings.DOSESPOT_API_URL)
    sso = create_sso_soap_object(clinician_id)

    resp = client.service.GetRefillRequestsTransmissionErrors(
            sso, clinician_id)

    return resp

def perform_pharmacy_search(zipcode, clinician_id=None):
    if clinician_id is None:
        clinician_id = settings.DOSESPOT_PROXY_USER_ID
        
    client = Client(url=WSDL, location=settings.DOSESPOT_API_URL)
    sso = create_sso_soap_object(clinician_id)
    
    try:
        resp = client.service.PharmacySearch(
                            SingleSignOn=sso, 
                            PharmacyNameSearch='', 
                            PharmacyCity='', 
                            PharmacyStateTwoLetters='', 
                            PharmacyZipCode=zipcode)
    except WebFault, e:
        logging.error(str(e))
        raise DoseSpotException()
        
    resp = recursive_asdict(resp)['Pharmacies']
    resp = clean_pharmacy_search_response(resp, zipcode)
    
    return resp
    
def validate_pharmacy_id(pharmacy_id, clinician_id=None):
    if clinician_id is None:
        clinician_id = settings.DOSESPOT_PROXY_USER_ID
    
    client = Client(url=WSDL, location=settings.DOSESPOT_API_URL)
    sso = create_sso_soap_object(clinician_id)
    
    try:
        resp = client.service.PharmacyValidate(
                            SingleSignOn=sso,
                            PharmacyId=int(pharmacy_id))
    except WebFault, e:
        logging.error(str(e))
        raise DoseSpotException('Error contacting host')
    else:
        if resp.Result.ResultCode == "ERROR":
            raise DoseSpotException('Invalid pharmacy ID')
        resp = recursive_asdict(resp)['Pharmacy']

    return resp
    
def clean_pharmacy_search_response(resp, zipcode):
    logger.debug(resp)
    pharmacies = [pharmacy for pharmacy in resp['Pharmacy'] if pharmacy['ZipCode'] == zipcode]
    return {'Pharmacy':pharmacies}
    
def recursive_asdict(d):
    """Convert Suds object into serializable format."""
    out = {}
    for k, v in asdict(d).iteritems():
        if hasattr(v, '__keylist__'):
            out[k] = recursive_asdict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(recursive_asdict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out

class DoseSpotException(Exception):
    pass
