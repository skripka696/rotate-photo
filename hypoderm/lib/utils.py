import logging

import braintree

from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from hypoderm.models import PatientProfile, ProviderProfile

logger = logging.getLogger(__name__)


def braintree_init():
    if settings.BRAINTREE_ENVIRONMENT == 'sandbox':
        logger.info('Initializing braintree sandbox environment')
        BRAINTREE_ENVIRONMENT = braintree.Environment.Sandbox
    elif settings.BRAINTREE_ENVIRONMENT == 'production':
        logger.info('Initializing braintree production environment')
        BRAINTREE_ENVIRONMENT = braintree.Environment.Production
    MERCHANT_ID = settings.BRAINTREE_MERCHANT_ID
    PUBLIC_KEY = settings.BRAINTREE_PUBLIC_KEY
    PRIVATE_KEY = settings.BRAINTREE_PRIVATE_KEY
    braintree.Configuration.configure(BRAINTREE_ENVIRONMENT,
                                      merchant_id=MERCHANT_ID,
                                      public_key=PUBLIC_KEY,
                                      private_key=PRIVATE_KEY)


def braintree_client_token(user):
    logger.info('Generating braintree client token')
    return braintree.ClientToken.generate({
    })


def braintree_submit_transaction(user, nonce, amount):
    logger.info('Submitting braintree payment, amount=%s nonce=%s' %
                (amount, nonce))
    result = braintree.Transaction.sale({
        'amount': amount,
        'payment_method_nonce': nonce
    })
    logger.info(
        'Received braintree transaction result, is_success=%s' %
        (result.is_success))

    if result.is_success:
        return result

    if result.errors.deep_errors:
        for error in result.errors.deep_errors:
            logger.error(
                'Transaction had an error: attribute=%s errorcode=%s message="%s"' %
                (error.attribute, error.code, error.message))
    return result

def full_registration_required(function):
    def wrap(request, *args, **kwargs):
        try:
            profile = getattr(request.user, 'patientprofile')
        except PatientProfile.DoesNotExist:
            try:
                profile = getattr(request.user, 'providerprofile')
            except ProviderProfile.DoesNotExist:
                logger.warn('User not fully registered; redirecting to setup page')
                return redirect('patient-setup')

        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__

    return wrap
