from django import forms
from hypoderm.lib.dosespot import validate_pharmacy_id, DoseSpotException
from django.template import Context
from django.template.loader import render_to_string
from django.utils.html import format_html


class PreferredPharmacyWidget(forms.widgets.NumberInput):
    input_type = 'hidden'

    def render(self, name, value, attrs=None):
        try:
            pharmacy_info = validate_pharmacy_id(value)
        except (TypeError, DoseSpotException) as e:
            pharmacy_info = None

        context = Context({
            'pharmacy': pharmacy_info,
        })

        html = ''
        if pharmacy_info:
            html = render_to_string('auth/common/pharmacy_details.html', context=context)

        html = format_html('{}{}', html, super(PreferredPharmacyWidget, self).render(name, value, attrs))

        return html
