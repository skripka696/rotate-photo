import logging

import requests

from requests.auth import HTTPBasicAuth
from django.conf import settings

logger = logging.getLogger(__name__)

INTERFAX_PROTO = 'https'
INTERFAX_HOST = 'rest.interfax.net'
INTERFAX_BASE = '%s://%s' % (INTERFAX_PROTO, INTERFAX_HOST)

def post_fax(payload, destination):
    auth = HTTPBasicAuth(settings.INTERFAX_USER, settings.INTERFAX_PASSWORD)

    headers = {
        'content-type': 'text/html',
    }

    params = {
        'faxNumber': destination,
    }

    logger.info("Sending fax to destination='%s'" % (destination,))

    r = requests.post('%s/outbound/faxes' % (INTERFAX_BASE,),
        auth=auth,
        headers=headers,
        params=params,
        data=payload,
    )

    logger.info("Fax sent; got response_code='%s'" % (r.status_code,))

    if r.status_code == 201:
        location = r.headers['location']
        transaction_id = location.split('/')[-1]

        logger.info(
            "Fax post successfully received, transaction_id='%s'" % \
            (transaction_id,)
        )

        return {
            'success' : True,
            'transaction_id': transaction_id,
            'raw_response': r,
        }

    logger.warn("Fax post rejected, reason='%s'" % (r.json()['code'],))

    return {
        'success': False,
        'reason': r.json(),
        'raw_response': r,
    }
