import json
import logging

from django.views.generic import View
from django.http import JsonResponse

logger = logging.getLogger(__name__)

class JSONAutoCompleteView(View):
    queryset = None
    field = None

    def get(self, request):
        partial = request.GET.get('q')

        fargs = { (self.field + '__icontains'): partial }
        q = self.queryset.filter(**fargs).values('id', self.field)

        payload = {
            'data': [ {'id': r['id'], self.field: r[self.field]} for r in q ]
        }

        return JsonResponse(payload, safe=False)
