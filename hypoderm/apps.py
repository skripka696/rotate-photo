from django.apps import AppConfig


class HypodermConfig(AppConfig):
    name = 'hypoderm'
    verbose_name = 'Hypoderm'

    def ready(self):
        import signals
