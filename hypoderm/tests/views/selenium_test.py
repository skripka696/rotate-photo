from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from django.conf import settings
import selenium.webdriver.support.ui as ui
from hypoderm.models import Contract
import hypoderm.models


from selenium import webdriver


class SeleniumTest(LiveServerTestCase):

    def __init__(self, *args, **kwargs):
        super(SeleniumTest, self).__init__(*args, **kwargs)
        settings.DEBUG=True

    def setUp(self):
        self.selenium = webdriver.Firefox()
        hypoderm.models.Contract.objects.create(contract_text='text', role='patient', is_active=True)
        hypoderm.models.BodyPart.objects.create(type=1, name='test', polygon='148,160,147,233,155,268,160,290,157,320,152,385,211,378,269,377,267,313,264,288,272,258,279,226,275,158,237,143,185,140')
        super(SeleniumTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTest, self).tearDown()

    def first(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/patient/signup/')


        #registr
        username = selenium.find_element_by_name("username")
        email = selenium.find_element_by_name("email")
        password = selenium.find_element_by_name('password')
        submit = selenium.find_element_by_class_name('btn-block')


        username.send_keys('22222222')
        email.send_keys('user2@email.com')
        password.send_keys('12345678')
        submit.send_keys(Keys.RETURN)

        # setup
        first_name = selenium.find_element_by_name("first_name")
        middle_name = selenium.find_element_by_name("middle_name")
        last_name = selenium.find_element_by_name("last_name")
        phone = selenium.find_element_by_name("phone")
        address_line1 = selenium.find_element_by_name("mail_one")
        address_line2 = selenium.find_element_by_name("mail_two")
        city = selenium.find_element_by_name("city")
        state = selenium.find_element_by_name("state")
        zipcode = selenium.find_element_by_name("zipcode")
        gender = selenium.find_element_by_name("gender")
        birth_date = selenium.find_element_by_name("birth_date")
        contract_agree = selenium.find_element_by_name("contract_agree")
        submit = selenium.find_element_by_class_name('btn-primary')


        first_name.send_keys("1111")
        middle_name.send_keys('11111')
        last_name.send_keys('1111')
        phone.send_keys('2602457885')
        address_line1.send_keys("asd@bvfd.com")
        address_line2.send_keys("asd@bvfd.com")
        city.send_keys('town')
        state.send_keys('Alabama')
        zipcode.send_keys('11111')
        gender.send_keys('Male')
        birth_date.send_keys('08/12/2015')
        contract_agree.send_keys(True)
        submit.send_keys(Keys.RETURN)

        #start visit first step
        submit = selenium.find_element_by_class_name('btn-primary').click()



        allergies_check = selenium.find_element_by_css_selector("input[name='has_allergies-check'][value='no']").click()
        taken_check = selenium.find_element_by_css_selector("input[name='medicines_taken-check'][value='no']").click()
        procedures_check = selenium.find_element_by_css_selector("input[name='surgical_procedures-check'][value='no']").click()
        reatments_check = selenium.find_element_by_css_selector("input[name='receiving_treatments-check'][value='no']").click()
        predisposition_check = selenium.find_element_by_css_selector("input[name='has_predisposition-check'][value='no']").click()
        body_weight = selenium.find_element_by_name('body_weight')
        employment_status = selenium.find_element_by_name('employment_status')
        skin_type = selenium.find_element_by_name('skin_type')
        submit = selenium.find_element_by_class_name('btn-primary')

        body_weight.send_keys('1')
        employment_status.send_keys('Employed')
        skin_type.send_keys('Normal')
        submit.send_keys(Keys.RETURN)

        #start visit two step
        description = selenium.find_element_by_name("description")
        symptoms_severity = selenium.find_element_by_name("symptoms_severity")
        condition_duration_days = selenium.find_element_by_name("condition_duration_days")
        wait = ui.WebDriverWait(self.selenium, 10)
        wait.until(lambda driver:selenium.find_element_by_name('g'))
        aggravator_check = selenium.find_element_by_css_selector("input[name='aggravator-check'][value='no']").click()
        prior_treatment_check = selenium.find_element_by_css_selector("input[name='prior_treatment-check'][value='no']").click()
        suspected_changes_check = selenium.find_element_by_css_selector("input[name='suspected_changes-check'][value='no']").click()
        additional_information_check = selenium.find_element_by_css_selector("input[name='additional_information-check'][value='no']").click()
        mapBack = selenium.find_element_by_xpath('//map[@id="mapBack"]/area').click()
        submit = selenium.find_element_by_class_name('btn-primary')

        description.send_keys('hello')
        symptoms_severity.send_keys('1')
        condition_duration_days.send_keys(1)
        submit.send_keys(Keys.RETURN)


        # selenium.wait_for_page_to_load("30")
        wait = ui.WebDriverWait(self.selenium, 10)
        wait.until(lambda driver:selenium.find_element_by_name('g'))
