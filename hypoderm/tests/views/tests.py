import localflavor.us

from faker import Faker

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import AnonymousUser, User, Group
from django.test import TestCase, Client

import hypoderm.signals
import hypoderm.models

import hypoderm.views.patient.views
import hypoderm.views.patient.visit

class AnonAuthTest(TestCase):
    def test_user_access(self):
        # Test anonymous user access.
        client = Client()

        response = client.get('/patient/home/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                'http://testserver/login/?next=/patient/home/')

        response = client.get('/provider/dashboard/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                'http://testserver/login/?next=/provider/dashboard/')

        response = client.get('/how-it-works/')

        self.assertEqual(response.status_code, 200)


class PatientAuthTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        fake = Faker()
        fake.seed(0)

        cls.user = User.objects.create_user(
            username=fake.user_name(),
            email=fake.free_email(),
            password='testtest',
        )

        cls.fake = fake
        
    def test_patient_signup(self):
        client = Client()
        client.login(username=self.user.username, password='testtest')
        
        params = {
            'username':'posttest',
            'password':'testpass',
            'newsletter_signup':False
        }
        
        response = client.post('/patient/signup/', params)
        self.assertEqual(response.status_code, 200)
        
        params['email'] = 'test@test.com'
        
        response = client.post('/patient/signup/', params)
        self.assertEqual(response.status_code, 302)
        db_user = User.objects.get(username=self.user.username)
        self.assertEqual(self.user, db_user)

    def test_redirect_partial_signup_to_setup(self):
        # Test partially-registered patient access.
        client = Client()
        client.login(username=self.user.username, password='testtest')

        response = client.get('/patient/home/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                'http://testserver/patient/setup/')

        response = client.get('/patient/visit/start')

        self.assertEqual(response.status_code, 403)

    def test_complete_user_access(self):
        # Test fully-registered patient access.
        states = dict([(s[1],s[0]) for s in localflavor.us.us_states.US_STATES])

        patient_profile = hypoderm.models.PatientProfile(
            user=self.user,
            first_name=self.fake.first_name(),
            middle_name=self.fake.first_name(),
            last_name=self.fake.last_name(),
            email=self.user.email,
            phone=self.fake.phone_number()[0:16],
            mail_one=self.fake.street_address(),
            mail_two='',
            city=self.fake.city(),
            state=states[self.fake.state()],
            zipcode=self.fake.postcode(),
            gender='M',
            birth_date=self.fake.date_time_ad(),
        )
        patient_profile.save()

        patient_group = Group.objects.get(name="patients")
        self.user.groups.add(patient_group)
        self.user.save()

        client = Client()
        client.login(username=self.user.username, password='testtest')

        response = client.get('/patient/home/')
        self.assertEqual(response.status_code, 200)

        response = client.get('/patient/visit/start')
        self.assertEqual(response.status_code, 200)

        response = client.get('/patient/setup/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                'http://testserver/home/')

        response = client.get('/provider/dashboard/')
        self.assertEqual(response.status_code, 403)

class ProviderAuthTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        fake = Faker()
        fake.seed(0)

        cls.user = User.objects.create_user(
            username=fake.user_name(),
            email=fake.free_email(),
            password='testtest',
        )

        cls.fake = fake

    def test_user_access(self):
        provider_profile = hypoderm.models.ProviderProfile(
            user=self.user,
        )
        provider_profile.save()

        provider_group = Group.objects.get(name="providers")
        self.user.groups.add(provider_group)
        self.user.save()

        client = Client()
        client.login(username=self.user.username, password='testtest')

        response = client.get('/provider/dashboard/')
        self.assertEqual(response.status_code, 200)

        response = client.get('/patient/home/')
        self.assertEqual(response.status_code, 403)

        response = client.get('/patient/visit/start')
        self.assertEqual(response.status_code, 403)
