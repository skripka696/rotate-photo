import localflavor.us

from faker import Faker
from suds.client import Client

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import AnonymousUser, User, Group
from django.test import TestCase, Client, override_settings
from django.conf import settings

import hypoderm.signals
import hypoderm.models

import hypoderm.views.patient.views
import hypoderm.views.patient.visit

@override_settings(DOSESPOT_PROXY_USER_ID='734')
class VisitTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        fake = Faker()
        fake.seed(0)
        cls.fake = fake

    def create_test_user(self, group):
        user = User.objects.create_user(
            username=self.fake.user_name(),
            email=self.fake.free_email(),
            password='testtest',
        )

        states = dict([(s[1],s[0]) for s in localflavor.us.us_states.US_STATES])

        if group == "patients":
            patient_profile = hypoderm.models.PatientProfile(
                user=user,
                first_name=self.fake.first_name(),
                middle_name=self.fake.first_name(),
                last_name=self.fake.last_name(),
                email=user.email,
                phone=self.fake.phone_number()[0:16],
                mail_one=self.fake.street_address(),
                mail_two='',
                city=self.fake.city(),
                state=states[self.fake.state()],
                zipcode=self.fake.postcode(),
                gender='M',
                birth_date=self.fake.date_time_this_decade(),
            )
            patient_profile.save()
        else:
            provider_profile = hypoderm.models.ProviderProfile(
                user=user,
                email=user.email,
                clinician_id=settings.DOSESPOT_PROXY_USER_ID,
            )
            provider_profile.save()

        user_group = Group.objects.get(name=group)
        user.groups.add(user_group)
        user.save()        
        
        return user

    def create_patient_condition_description(self, user):
        medical_record = hypoderm.models.PatientMedicalRecord.objects.create(patient=user.patientprofile, body_weight=123, employment_status='S', skin_type='N')
        payment_record = hypoderm.models.PatientPaymentRecord.objects.create(payment_amount=0, transaction_id='0')
        visit = hypoderm.models.PatientVisit.objects.create(patient=user.patientprofile, medical_record=medical_record, payment_record=payment_record)
        condition = hypoderm.models.PatientConditionDescription.objects.create(patient_visit=visit, description='Test', symptoms_severity=0)
        
        return condition            