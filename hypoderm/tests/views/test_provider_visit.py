import localflavor.us

from faker import Faker
from mock import patch

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import AnonymousUser, User, Group
from django.test import TestCase, Client, override_settings

from hypoderm.tests.views.common import VisitTestCase

import hypoderm.signals
import hypoderm.models

import hypoderm.views.patient.views
import hypoderm.views.patient.visit

class MockPostResponse(object):
    status_code = 302
    headers = {
        'location': 'http://test.com/?q=1'
    }
    url = 'http://test.com'

class MockInterfaxResponse(object):
    status_code = 200

def mock_post_request(*args, **kwargs):
    return MockPostResponse()

class ProviderVisitWizardTest(VisitTestCase):
    def test_provider_visit(self):
        provider = self.create_test_user("providers")
        patient = self.create_test_user("patients")
        condition = self.create_patient_condition_description(patient)
        
        client = Client()
        client.login(username=provider.username, password='testtest')
        
        with patch('requests.post', side_effect=mock_post_request):
            response = client.get('/provider/visit/%s/' % condition.patient_visit.pk)
            self.assertEqual(response.status_code, 200)
            
        patient.patientprofile.dosespot_id = 1
        response = client.get('/provider/visit/%s/' % condition.patient_visit.pk)
        self.assertEqual(response.status_code, 200)

        params = {
            'destination_name':'Test',
            'destination':'Test',
            'text':'Test'
        }
        
        interfax_return = {
            'success': True,
            'transaction_id': 123,
            'raw_response': MockInterfaxResponse()
        }
        
        with patch('hypoderm.lib.interfax.post_fax', return_value=interfax_return):
            response = client.post('/provider/visit/%s/faxreferral' % condition.patient_visit.pk, params)
            self.assertEqual(response.status_code, 302)
        
        client.login(username=patient.username, password='testtest')
        response = client.get('/provider/visit/%s/' % condition.patient_visit.pk)
        self.assertEqual(response.status_code, 403)

