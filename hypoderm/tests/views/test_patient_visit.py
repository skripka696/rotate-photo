import localflavor.us

from faker import Faker
from mock import patch
from logging import getLogger

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import AnonymousUser, User, Group
from django.test import TestCase, Client
from hypoderm.tests.views.common import VisitTestCase

import hypoderm.signals
import hypoderm.models

import hypoderm.views.patient.views
import hypoderm.views.patient.visit

logger = getLogger(__name__)

class PatientVisitWizardTest(VisitTestCase):
    def test_patient_visit_no_condition(self):
        user1 = self.create_test_user("patients")
        
        client = Client()
        client.login(username=user1.username, password='testtest')
        
        response = client.get('/patient/visit/start')
        self.assertEqual(response.status_code, 200)        

        response = client.get('/patient/visit/condition')
        self.assertEqual(response.status_code, 200)        
  
    def test_patient_visit_correct_user(self):
        user1 = self.create_test_user("patients")
        condition1 = self.create_patient_condition_description(user1)

        client = Client()
        client.login(username=user1.username, password='testtest')

        response = client.get('/patient/visit/photos')
        self.assertEqual(response.status_code, 200)

        with patch('hypoderm.views.patient.visit.braintree_client_token',
                    return_value=''):
            response = client.get('/patient/visit/review')
            self.assertEqual(response.status_code, 200)     
        
        response = client.get('/patient/visit/%s/' % condition1.patient_visit.pk)
        self.assertEqual(response.status_code, 200) 

        response = client.get('/patient/visit/%s/status' % condition1.patient_visit.pk)
        self.assertEqual(response.status_code, 200) 
        
    def test_patient_visit_incorrect_user(self):
        user1 = self.create_test_user("patients")
        user2 = self.create_test_user("patients")
        condition2 = self.create_patient_condition_description(user2)

        client = Client()
        client.login(username=user1.username, password='testtest')        
        
        response = client.get('/patient/visit/%s/' % condition2.patient_visit.pk)
        self.assertEqual(response.status_code, 302) 

        response = client.get('/patient/visit/%s/status' % condition2.patient_visit.pk)
        self.assertEqual(response.status_code, 403)
                
    def test_patient_create_visit(self):
        user = self.create_test_user("patients")
        client = Client()
        client.login(username=user.username, password='testtest')
        
        params = {
            'has_allergies': 'None',
            'medicines_taken': 'None',
            'surgical_procedures': 'None',
            'receiving_treatments': 'None',
            'has_predisposition': 'None',
            'body_weight': 123,
            'employment_status': 'S',
            'skin_type': 'N'
        }
        
        prev_count = hypoderm.models.PatientVisit.objects.all().count()
        response = client.post('/patient/visit/start', params)
        self.assertEqual(response.status_code, 302)
        new_count = hypoderm.models.PatientVisit.objects.all().count()
        self.assertEqual(prev_count + 1, new_count)        
        
        