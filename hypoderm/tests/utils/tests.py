import random
import hashlib
import base64
import urllib
import json
import localflavor
import datetime

from faker import Faker
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, override_settings, RequestFactory
from django.conf import settings
from django.contrib.auth.models import User, Group

from hypoderm.lib import dosespot, views
from hypoderm.models import Education, PatientProfile

@override_settings(DOSESPOT_CLINIC_ID='', DOSESPOT_CLINIC_KEY='', DOSESPOT_USER_ID='', DOSESPOT_SSO_URL='http://my.staging.dosespot.com/LoginSingleSignOn.aspx')
class DoseSpotSSOTestCase(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        fake = Faker()
        fake.seed(0)

        cls.user = User.objects.create_user(
            username=fake.user_name(),
            email=fake.free_email(),
            password='testtest',
        )

        cls.fake = fake
        
    def create_patient_profile_with_id(self):
        states = dict([(s[1],s[0]) for s in localflavor.us.us_states.US_STATES])

        patient_profile = PatientProfile(
            user=self.user,
            first_name=self.fake.first_name(),
            middle_name=self.fake.first_name(),
            last_name=self.fake.last_name(),
            email=self.user.email,
            phone=self.fake.phone_number()[0:16],
            mail_one=self.fake.street_address(),
            mail_two='',
            city=self.fake.city(),
            state=states[self.fake.state()],
            zipcode=self.fake.postcode(),
            gender='M',
            birth_date=datetime.datetime(1990, 10, 10),
            dosespot_id=1,
        )
        patient_profile.save()

        return patient_profile    
        
    def test_generate_sso_code_raw(self):
        test_code = 'i0VpEBOWfbZAVaBSo63bbH6xnAbnBEoomVMzTKoO10Dyxgg97Kyhij+a41kMUDICp1y2OFrDreyFzn7T176Q9eKVbkGwCy7QRHdtor5qykrtgHWu4ksVtw'
        random.seed(1)
        phrase, sso_code = dosespot.generate_sso_code()
        self.assertEqual(test_code, sso_code)
    
    def test_generate_sso_code_urlencode(self):
        test_code = urllib.quote_plus('i0VpEBOWfbZAVaBSo63bbH6xnAbnBEoomVMzTKoO10Dyxgg97Kyhij+a41kMUDICp1y2OFrDreyFzn7T176Q9eKVbkGwCy7QRHdtor5qykrtgHWu4ksVtw')
        random.seed(1)
        phrase, url_encoded_sso_code = dosespot.generate_sso_code(url_encode=True)
        
        self.assertEqual(test_code, url_encoded_sso_code)
    
    def test_generate_sso_userid_verify_raw(self):
        test_code = 'TXC6Qu5Is4J5d2HR6ejb73sl5hPml/1kDs4qA5urWIhxevH5bwl5SKgRBPce8R8Swpe+VPZq2/Ddlc9abLVbVQ'
        random.seed(1)
        sso_phrase, _ = dosespot.generate_sso_code()
        verification_code = dosespot.generate_sso_userid_verify(sso_phrase, settings.DOSESPOT_USER_ID)
        
        self.assertEqual(test_code, verification_code)
        
    def test_generate_sso_userid_verify_urlencode(self):
        test_code = urllib.quote_plus('TXC6Qu5Is4J5d2HR6ejb73sl5hPml/1kDs4qA5urWIhxevH5bwl5SKgRBPce8R8Swpe+VPZq2/Ddlc9abLVbVQ')
        random.seed(1)
        sso_phrase, _ = dosespot.generate_sso_code()
        verification_code = dosespot.generate_sso_userid_verify(sso_phrase, settings.DOSESPOT_USER_ID, url_encode=True)
        
        self.assertEqual(test_code, verification_code)

    def test_get_provider_page(self):
        random.seed(1)
        test_url = 'http://my.staging.dosespot.com/LoginSingleSignOn.aspx?b=2&SingleSignOnUserIdVerify=TXC6Qu5Is4J5d2HR6ejb73sl5hPml%2F1kDs4qA5urWIhxevH5bwl5SKgRBPce8R8Swpe%2BVPZq2%2FDdlc9abLVbVQ&SingleSignOnClinicId=&SingleSignOnCode=i0VpEBOWfbZAVaBSo63bbH6xnAbnBEoomVMzTKoO10Dyxgg97Kyhij%2Ba41kMUDICp1y2OFrDreyFzn7T176Q9eKVbkGwCy7QRHdtor5qykrtgHWu4ksVtw&SingleSignOnUserId=&RefillsErrors=1&SingleSignOnPhraseLength=32'
        url = dosespot.get_provider_page(settings.DOSESPOT_USER_ID)
        
        self.assertEqual(test_url, url)
        
    def test_get_patient_redirect_with_id(self):
        profile = self.create_patient_profile_with_id()
        random.seed(1)
        test_url = 'http://my.staging.dosespot.com/LoginSingleSignOn.aspx?City=Rollandview&FirstName=Jacquez&LastName=Green&PatientID=1&DateOfBirth=10%2F10%2F1990&State=MT&SingleSignOnUserIdVerify=TXC6Qu5Is4J5d2HR6ejb73sl5hPml%2F1kDs4qA5urWIhxevH5bwl5SKgRBPce8R8Swpe%2BVPZq2%2FDdlc9abLVbVQ&SingleSignOnClinicId=&Address1=58855+Johny+Squares+Apt.+157&Address2=&PrimaryPhone=%2B85%286%291981377948&PharmacyId=&SingleSignOnPhraseLength=32&MiddleName=Adria&Gender=Male&SingleSignOnCode=i0VpEBOWfbZAVaBSo63bbH6xnAbnBEoomVMzTKoO10Dyxgg97Kyhij%2Ba41kMUDICp1y2OFrDreyFzn7T176Q9eKVbkGwCy7QRHdtor5qykrtgHWu4ksVtw&MRN=&PhoneAdditionalType1=&PhoneAdditionalType2=&PhoneAdditional1=&PhoneAdditional2=&b=2&Suffix=&PrimaryPhoneType=Cell&ZipCode=53400-7953&SingleSignOnUserId=&Prefix='
        url = dosespot.get_patient_redirect(profile, settings.DOSESPOT_USER_ID)
        
        self.assertEqual(test_url, url)
        

class ViewTestCase(TestCase):
    def test_autocomplete_nomatch(self):
        e = Education(title="foobar", text="hello world!")
        e.save()

        class EducationAutoComplete(views.JSONAutoCompleteView):
            queryset = Education.objects.all()
            field = 'title'

        factory = RequestFactory()
        request = factory.get('/dumb/url?q=nomatch')
        request.user = AnonymousUser()

        response = EducationAutoComplete.as_view()(request)
        payload = json.loads(response.content)

        self.assertEqual(len(payload['data']), 0)

    def test_autocomplete_match(self):
        e = Education(title="foobar", text="hello world!")
        e.save()
        e = Education(title="moomoo", text="hello world!")
        e.save()

        class EducationAutoComplete(views.JSONAutoCompleteView):
            queryset = Education.objects.all()
            field = 'title'

        factory = RequestFactory()
        request = factory.get('/dumb/url?q=oob')
        request.user = AnonymousUser()

        response = EducationAutoComplete.as_view()(request)
        payload = json.loads(response.content)

        self.assertEqual(len(payload['data']), 1)
        self.assertEqual(payload['data'][0]['title'], 'foobar')
