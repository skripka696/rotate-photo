from django.views.generic.base import TemplateView
from django.views.generic import DetailView
from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response

from hypoderm.models import Contract

def index(request):
    context = RequestContext(request)

    if request.user.is_authenticated():
        return redirect('home')

    return render(request, 'unauth/index.html', context)

class QualityCareView(TemplateView):
    template_name = 'unauth/quality-care.html'

class HowItWorksView(TemplateView):
    template_name = 'unauth/how-it-works.html'

class AboutUsView(TemplateView):
    template_name = 'unauth/about-us.html'


class ContractView(DetailView):
    context_object_name = 'contract'
    template_name = 'unauth/contract.html'
    model = Contract