import logging

from django.views.generic import View
from django.http import HttpResponse
from braces.views import CsrfExemptMixin

from hypoderm.models import FaxReferral, FaxTransactionReceipt

logger = logging.getLogger(__name__)

class Callback(CsrfExemptMixin, View):
    def post(self, request):
        logger.info('Got a fax callback, payload="%s"' % (request.body,))

        transaction_id = request.POST['transaction_id']
        try:
            fax_referral = FaxReferral.objects.get(transaction_id=transaction_id)
        except FaxReferral.DoesNotExist:
            logger.error(
                "fax transaction_id='%s' does not exist" % (transaction_id,))
            return HttpResponse(status=400)

        fax_transaction = \
            FaxTransactionReceipt(raw_return_payload=request.body)
        fax_transaction.save()

        fax_referral.receipt = fax_transaction
        fax_referral.save()

        return HttpResponse()
