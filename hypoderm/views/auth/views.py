import logging
import mimetypes

import requests

from django.http import HttpResponse, FileResponse
from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist

from braces.views import LoginRequiredMixin, UserPassesTestMixin

from hypoderm.models import (
    PatientProfile,
    ProviderProfile,
    Notification,
    PatientConditionDescription,
    Photo,
    EmailPreferences,
)

logger = logging.getLogger(__name__)

@login_required
def home(request):
    context = RequestContext(request)

    try:
        patient_profile = request.user.patientprofile
    except PatientProfile.DoesNotExist:
        patient_profile = None

    try:
        provider_profile = request.user.providerprofile
    except ProviderProfile.DoesNotExist:
        provider_profile = None

    if patient_profile:
        return redirect('patient-home')
    elif provider_profile:
        return redirect('provider-dashboard')

    # didn't find a patient profile or a provider profile
    # so assume this is a patient who is not fully signed up
    return redirect('patient-setup')
        
class Notifications(LoginRequiredMixin, View):
    def get(self, request):
        data = []
        for notification in Notification.objects.filter(is_active=True, user=request.user).order_by('-created'):
            data.append({
                'id': notification.id,
                'message': notification.message,
                'created': notification.created,
                'link': notification.link
            })
        return JsonResponse(data, safe=False)

    def post(self, request, notification_id):
        notification = Notification.objects.get(pk=notification_id)
        notification.is_active = False
        notification.save()
        return redirect('notifications')

class PhotoProxy(LoginRequiredMixin, UserPassesTestMixin, View):
    raise_exception = True
    
    def test_func(self, user):
        try:
            photo = Photo.objects.get(id=self.kwargs['photo_id'])
        except ObjectDoesNotExist:
            return False
        else:
            return user.groups.filter(name="providers").exists() or photo.condition_description.patient_visit.patient == self.request.user.patientprofile
        
    def get(self, request, **kwargs):
        photo = Photo.objects.get(id=kwargs['photo_id'])
        content_type, _ = mimetypes.guess_type(photo.photo_data.name)
        r = requests.get(photo.photo_data.url, stream=True)
        # logger.info("%s" % photo.photo_data.url)
        
        return FileResponse(r.content, content_type=content_type)
