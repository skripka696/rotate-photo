import logging

from django.core import serializers
from django.views.generic import View
from django.http import JsonResponse
from django.shortcuts import HttpResponse

from braces.views import LoginRequiredMixin, UserPassesTestMixin

from hypoderm.models import (
    PatientVisit,
    VisitComment,
    PatientProfile,
)

from hypoderm.forms import (
    VisitCommentForm,
)

from hypoderm.signals import patient_visit_comment

logger = logging.getLogger(__name__)

class Comments(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self, user):
        return user.has_perms(["hypoderm.add_visitcomment", "hypoderm.view_patient_visit"]) and (user.groups.filter(name="providers").exists() or PatientVisit.objects.get(id=self.kwargs['visit_id']).patient.user == user)
    
    def get(self, request, visit_id):
        patient_visit = PatientVisit.objects.get(id=visit_id)

        comments = VisitComment.objects.filter(visit=patient_visit, is_deleted=False)

        comment_payload = []
        for c in comments:
            profile = c.user.patientprofile if hasattr(c.user, 'patientprofile') else None
            if profile:
                name = '%s %s' % (profile.first_name, profile.last_name)
            else:
                name = '%s' % c.user.username

            comment_payload.append({
                'text': c.text,
                'created': c.created.strftime("%A, %B %-d, %Y at %-I:%M %p"),
                'author_name': name,
            })

        payload = {
            'data': comment_payload
        }

        return JsonResponse(payload, safe=False)

    def post(self, request, visit_id):
        visit_comment_form = VisitCommentForm(request.POST)

        if visit_comment_form.is_valid():
            visit_comment = visit_comment_form.save(commit=False)

            visit_comment.user = request.user
            visit_comment.visit = PatientVisit.objects.get(id=visit_id)

            visit_comment.save()

            patient_visit_comment.send(self.__class__, visit_comment=visit_comment)

            return self.get(request, visit_id)
        else:
            return HttpResponse(status=400)
