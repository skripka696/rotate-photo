import logging

from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User, Group
from django.views.generic import View
from braces.views import LoginRequiredMixin

from hypoderm import signals

from hypoderm.forms import (
    PartialProviderSignupForm,
    SecurityQuestionForm,
)

from hypoderm.models import (
    EmailPreferences,
    ProviderProfile,
    Contract,
    ProviderConsentRecord,
)

logger = logging.getLogger(__name__)

class Partial(View):

    def get(self, request):
        if request.user.is_authenticated():
            return redirect('home')

        context = RequestContext(request)

        contract = Contract.objects.filter(role='provider', is_active=True).latest('created')

        form = PartialProviderSignupForm(initial={'contract': contract.pk})
        context.push({
            'form': form,
        })

        return render(request, 'unauth/signup/provider/partial.html', context)

    def post(self, request):
        context = RequestContext(request)

        form = PartialProviderSignupForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['username'],
                form.cleaned_data['email'],
                form.cleaned_data['password']
            )

            provider_group = Group.objects.get(name="providers")
            user.groups.add(provider_group)
            user.save()
            signals.provider_created.send(sender=self.__class__, user=user)

            ep = EmailPreferences(
                user=user,
                receive_newsletter=form.cleaned_data['newsletter_signup']
            )
            ep.save()

            user = authenticate(username=user.username,
                                password=form.cleaned_data['password'])
            profile = ProviderProfile(user=user, email=user.email)
            profile.save()

            contract_id = form.cleaned_data.get('contract')
            if contract_id:
                contract = Contract.objects.get(pk=contract_id)
                ProviderConsentRecord(provider=profile, contract=contract).save()
            if user is not None:
                login(request, user)
                return redirect('provider-dashboard')
            else:
                # we should never be here
                logger.error('Failed to create user')
        else:
            context.push({'form':form})

            return render(request, 'unauth/signup/provider/partial.html', context)
