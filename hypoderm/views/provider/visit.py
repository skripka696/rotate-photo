import logging
import datetime

from django.conf import settings
from django.template import RequestContext, Context
from django.template.loader import get_template
from django.views.generic import View
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.http import JsonResponse

import json

from braces.views import LoginRequiredMixin, MultiplePermissionsRequiredMixin

from hypoderm.models import (
    PatientVisit,
    Photo,
    PatientConditionDescription,
    PatientDiagnosis,
    Diagnosis,
)

from hypoderm.models import FaxReferral as FaxReferralModel

from hypoderm.forms import (
    PatientMedicalRecordForm,
    PatientConditionDescriptionForm,
    PhotoForm,
    ReferralFaxForm,
)

from hypoderm.lib import interfax, dosespot

logger = logging.getLogger(__name__)

class Index(LoginRequiredMixin, MultiplePermissionsRequiredMixin, View):
    raise_exception=True

    permissions = {
        'all': ["hypoderm.view_patient_visit", "hypoderm.administer_patient_visit"]
    }

    def get(self, request, visit_id):
        context = {}

        patient_visit = PatientVisit.objects.get(id=visit_id)
        medical_record = patient_visit.medical_record
        condition_description = PatientConditionDescription.objects.get(
            patient_visit=patient_visit)
        photos = Photo.objects.filter(condition_description=condition_description)

        medical_record_form = PatientMedicalRecordForm(instance=medical_record)
        condition_description_form = PatientConditionDescriptionForm(
            instance=condition_description)

        referral_form = ReferralFaxForm()

        context['visit'] = patient_visit
        context['age'] = datetime.datetime.now().year - patient_visit.patient.birth_date.year
        context['medical_record_form'] = medical_record_form
        context['condition_description_form'] = condition_description_form
        context['photos'] = photos
        context['referral_form'] = referral_form

        provider_profile = request.user.providerprofile

        context['dosespot_url'] = 'localhost'
        # dosespot.get_patient_redirect(patient_visit.patient,
        #         str(provider_profile.clinician_id))

        return render(request, 'auth/provider/visit/visit.html',
                RequestContext(request, context))

class FaxReferral(LoginRequiredMixin, MultiplePermissionsRequiredMixin, View):
    raise_exception=True

    permissions = {
        'all': ["hypoderm.view_patient_visit", "hypoderm.administer_patient_visit"]
    }

    def post(self, request, visit_id):

        referral_form = ReferralFaxForm(request.POST)

        if referral_form.is_valid():
            context = {}

            patient_visit = PatientVisit.objects.get(id=visit_id)
            medical_record = patient_visit.medical_record
            condition_description = PatientConditionDescription.objects.get(
                patient_visit=patient_visit)
            photos = Photo.objects.filter(
                condition_description=condition_description)

            medical_record_form = PatientMedicalRecordForm(
                instance=medical_record)
            condition_description_form = PatientConditionDescriptionForm(
                instance=condition_description)
            fdata = referral_form.cleaned_data


            context['user'] = request.user
            context['visit'] = patient_visit
            context['medical_record_form'] = medical_record_form
            context['condition_description_form'] = condition_description_form
            context['photos'] = photos
            context['referral'] = fdata

            fax_template = get_template(
                'auth/provider/visit/fax-referral.html')

            payload = fax_template.render(Context(context))

            fax_referral = FaxReferralModel(
                requestor=request.user,
                destination=fdata['destination'],
                text=payload,
                destination_name=fdata['destination_name'] or None,
                specialty=fdata['specialty'] or None,
                phone=fdata['phone'] or None,
            )
            fax_referral.save()

            result = interfax.post_fax(
                payload,
                fdata['destination'],
            )

            if result['success']:
                fax_referral.transaction_id = result['transaction_id']
                messages.success(request, 'Fax referral sent.')
            else:
                fax_referral.error_json = result['raw_response'].text
                messages.error(request, 'Failed to send fax referral. The problem has been logged and will be addressed ASAP.')

            fax_referral.http_status_code = result['raw_response'].status_code
            fax_referral.save()
        else:
            messages.error(request, "There's something invalid about your fax referral: ")

        return redirect('provider-visit', visit_id=visit_id)


class Diagnoses(LoginRequiredMixin, MultiplePermissionsRequiredMixin, View):
    permissions = {
        'all': ["hypoderm.view_patient_visit", "hypoderm.administer_patient_visit"]
    }

    def post(self, request, visit_id):
        diagnoses = json.loads(request.body)
        visit = get_object_or_404(PatientVisit, pk=visit_id)

        PatientDiagnosis.objects.filter(visit=visit).delete()

        for diagnosis_id in diagnoses:
            diagnosis = get_object_or_404(Diagnosis, pk=diagnosis_id)
            PatientDiagnosis(visit=visit, diagnosis=diagnosis).save()

        return JsonResponse({'success': True}, safe=False)
