import logging

from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages

from braces.views import (
    LoginRequiredMixin,
    MultiplePermissionsRequiredMixin,
    UserPassesTestMixin,
)

from hypoderm.lib.utils import full_registration_required

from hypoderm.lib.views import JSONAutoCompleteView

from hypoderm.models import (
    PatientVisit,
    Diagnosis,
)

from hypoderm.forms import ProviderProfileSettingsForm

@login_required
@full_registration_required
@permission_required("hypoderm.view_provider_portal", raise_exception=True)
def dashboard(request):
    context = RequestContext(request)

    patient_visits = PatientVisit.objects.filter(payment_record__isnull=False)
    context.push({"visits": patient_visits})

    return render_to_response("auth/provider/dashboard.html", context)

class DiagnosisAutoComplete(LoginRequiredMixin, MultiplePermissionsRequiredMixin, JSONAutoCompleteView):
    queryset = Diagnosis.objects.all()
    field = 'name'

    permissions = {
        'all': ["hypoderm.view_patient_visit", "hypoderm.administer_patient_visit"]
    }

class ProviderProfileSettings(LoginRequiredMixin, UserPassesTestMixin, View):
    def test_func(self, user):
        user.groups.filter(name='providers').exists()
        
    def get(self, request):
        context = RequestContext(request)
        user = request.user
        email_preferences = user.emailpreferences
        form = ProviderProfileSettingsForm(initial={'email':user.email, 'receive_newsletter':email_preferences.receive_newsletter})
        
        context.push({'settings_form':form})

        return render(request, 'auth/common/settings.html', context)
        
    def post(self, request):
        context = RequestContext(request)
        form = ProviderProfileSettingsForm(request.POST)
        user = request.user
        
        if form.is_valid():
            profile = user.providerprofile

            email_preferences = user.emailpreferences
            
            user.email = form.cleaned_data['email']
            user.save()
            
            profile.email = form.cleaned_data['email']
            profile.save()
            
            email_preferences.receive_newsletter = form.cleaned_data['receive_newsletter']
            email_preferences.save()
            
            return redirect('provider-profile-settings')
        else:
            context.push({'settings_form':form})
            return render(request, 'auth/common/settings.html', context)
