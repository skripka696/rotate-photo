import logging
import datetime

from collections import defaultdict

from django.template import RequestContext
from django.views.generic import View
from django.shortcuts import render, redirect, render_to_response
from django.forms.formsets import formset_factory
from django.utils.text import slugify
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.http.response import JsonResponse

from braces.views import LoginRequiredMixin, UserPassesTestMixin

from hypoderm.forms import (
    PatientMedicalRecordForm,
    PatientConditionDescriptionForm,
    PhotoForm,
    VisitDiscountCodeForm,
    PatientProfileSettingsForm,
)

from hypoderm.models import (
    PatientVisit,
    Photo,
    PatientConditionDescription,
    PatientPaymentRecord,
    VisitDiscount
)

from hypoderm.lib.utils import (
    braintree_init,
    braintree_client_token,
    braintree_submit_transaction,
)

from hypoderm.lib.dosespot import validate_pharmacy_id, DoseSpotException

logger = logging.getLogger(__name__)

braintree_init()

def patient_visit_test_func(self, user):
    return user.is_active and user.has_perm("hypoderm.view_patient_visit") and not user.groups.filter(name="providers").exists()

def patient_visit_specific_test_func(self, user):
    return patient_visit_test_func(self, user) and PatientVisit.objects.filter(patient=user.patientprofile, pk=self.request.resolver_match.kwargs['visit_id']).exists()

class Index(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_specific_test_func
    
    def get(self, request, visit_id):
        context = {}

        patient_visit = PatientVisit.objects.get(id=visit_id)
        medical_record = patient_visit.medical_record
        condition_description = PatientConditionDescription.objects.get(
            patient_visit=patient_visit)
        photos = Photo.objects.filter(condition_description=condition_description)

        medical_record_form = PatientMedicalRecordForm(instance=medical_record)
        condition_description_form = PatientConditionDescriptionForm(
            instance=condition_description)

        context['visit'] = patient_visit
        context['age'] = datetime.datetime.now().year - patient_visit.patient.birth_date.year
        context['medical_record_form'] = medical_record_form
        context['condition_description_form'] = condition_description_form
        context['photos'] = photos

        return render(request, 'auth/patient/visit/visit.html',
                      RequestContext(request, context))

class Start(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def get(self, request):
        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()

        if active_visit:
            context.push({'medical_record_form':PatientMedicalRecordForm(instance=active_visit.medical_record)})
            context.push({'override':'new_visit'})
        else:
            context.push({'medical_record_form':PatientMedicalRecordForm()})

        return render(request, 'auth/patient/visit/index.html', context)

    def post(self, request):
        context = RequestContext(request)

        try:
            active_visit = request.user.patientprofile.get_active_visit()
            medical_record = active_visit.medical_record
            medical_record_form = PatientMedicalRecordForm(
                    instance=medical_record,
                    data=request.POST)
        except:
            active_visit = None
            medical_record_form = PatientMedicalRecordForm(data=request.POST)

        if medical_record_form.is_valid():
            medical_record = medical_record_form.save(commit=False)

            medical_record.patient = request.user.patientprofile
            medical_record.save()

            if not active_visit:
                patient_visit = PatientVisit(
                    patient=request.user.patientprofile,
                    medical_record=medical_record,
                    is_active=True,
                    is_deleted=False
                )
                patient_visit.save()

            medical_record_form.save_m2m()

            logger.info(
                'Persisted new visit. Will now capture condition info.')

            return redirect('patient-visit-condition')
        else:
            context.push({'medical_record_form':medical_record_form})
            return render(request, 'auth/patient/visit/index.html', context)

class Condition(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def get(self, request):
        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()

        try:
            condition_description = PatientConditionDescription.objects.get(
                patient_visit=active_visit)

            context.push({'condition_form':PatientConditionDescriptionForm(instance=condition_description)})
            context.push({'override':'record_captured'})
        except PatientConditionDescription.DoesNotExist:
            context.push({'condition_form':PatientConditionDescriptionForm()})

        return render(
            request, 'auth/patient/visit/condition_capture.html', context)

    def post(self, request):
        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()

        try:
            condition_description = PatientConditionDescription.objects.get(
                patient_visit=active_visit)
            logger.info('Found existing condition description.')

            context.push({
                'condition_form': PatientConditionDescriptionForm(
                    instance=condition_description,
                    data=request.POST),
            })
        except PatientConditionDescription.DoesNotExist:
            context.push({
                'condition_form': PatientConditionDescriptionForm(
                    data=request.POST),
            })

        if context['condition_form'].is_valid():
            condition_description = context[
                'condition_form'].save(commit=False)
            condition_description.patient_visit = \
                request.user.patientprofile.get_active_visit()
            condition_description.save()
            context['condition_form'].save_m2m()

            logger.info(
                'Captured condition information. Will now capture photos.')

            return redirect('patient-visit-photos')
        else:
            return render(
                request, 'auth/patient/visit/condition_capture.html', context)

class Photos(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def get(self, request):
        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()
        condition_description = PatientConditionDescription.objects.get(
            patient_visit=active_visit)
        body_parts = condition_description.affected_body_parts.all()

        PhotoFormSet = formset_factory(
                PhotoForm,
                extra=0,
                )

        formsets = {}
        photos = Photo.objects.filter(condition_description=condition_description)
        if photos:
            context.push({'override':'conditions_captured'})
            logger.debug('Found %d existing photos.' % (len(photos)))

            photos_by_body_part = defaultdict(list)
            for photo in photos:
                initial_form = PhotoForm(instance=photo)
                photos_by_body_part[photo.body_part].append(initial_form.initial)
            for body_part in photos_by_body_part:
                formsets[body_part] = \
                    PhotoFormSet(initial=photos_by_body_part[body_part],
                            prefix=slugify(body_part.name))
        else:
            for body_part in body_parts:
                formsets[body_part] = PhotoFormSet(initial=[{
                    'body_part': body_part,
                }], prefix=slugify(body_part.name))


        context.push({
                'photo_formsets':formsets,
        })

        return render(
            request, 'auth/patient/visit/photo_capture.html', context)

    def post(self, request):
        logger.debug('Persisting photos.')

        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()
        condition_description = PatientConditionDescription.objects.get(
            patient_visit=active_visit)
        body_parts = condition_description.affected_body_parts.all()

        PhotoFormSet = formset_factory(PhotoForm)
        formsets = []
        bad_formsets = []

        bad_formset = False
        for body_part in body_parts:
            formset = PhotoFormSet(
                request.POST, request.FILES, prefix=slugify(body_part.name))
            #import pdb; pdb.set_trace()
            if not formset.is_valid():
                bad_formsets.append(formset)
                break
            formsets.append(formset)


        if bad_formsets:
            logger.debug("%d formsets were invalid." % (len(bad_formsets)))
            context.push({'photo_formsets': bad_formsets})
            return render(
                request, 'auth/patient/visit/photo_capture.html', context)

        logger.debug("%d formsets to be persisted." % (len(formsets)))

        for formset in formsets:
            for form in formset:
                m = form.save(commit=False)
                m.condition_description = condition_description
                m.save()

        return redirect('patient-visit-review')

class Review(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def get(self, request):
        context = {}

        active_visit = request.user.patientprofile.get_active_visit()
        medical_record = active_visit.medical_record
        condition_description = PatientConditionDescription.objects.get(
            patient_visit=active_visit)
        photos = Photo.objects.filter(condition_description=condition_description)

        medical_record_form = PatientMedicalRecordForm(instance=medical_record)
        condition_description_form = PatientConditionDescriptionForm(
            instance=condition_description)

        context['medical_record_form'] = medical_record_form
        context['condition_description_form'] = condition_description_form
        context['braintree_client_token'] = braintree_client_token(
                request.user)
        context['photos'] = photos
        context['override'] = 'pending_confirmation'

        payment_amount = settings.BASE_PAYMENT_AMOUNT
        if not active_visit.discount:
            context['discount_code_form'] = VisitDiscountCodeForm()
        else:
            context['discount'] = active_visit.discount
            payment_amount -= active_visit.discount.discount_amount

        context['base_payment'] = settings.BASE_PAYMENT_AMOUNT
        context['payment_amount'] = max(payment_amount, 0)

        try:
            pharmacy_info = validate_pharmacy_id(request.user.patientprofile.preferred_pharmacy_id)
            context['pharmacy'] = pharmacy_info
        except (TypeError, DoseSpotException) as e:
            logger.debug(str(e))

        return render(request, 'auth/patient/visit/confirm.html', RequestContext(request, context))

    def post(self, request):
        context = RequestContext(request)

        active_visit = request.user.patientprofile.get_active_visit()

        # calculate payment with discount
        payment_amount = settings.BASE_PAYMENT_AMOUNT
        if active_visit.discount:
            payment_amount -= active_visit.discount.discount_amount
            payment_amount = max(payment_amount, 0)

        active_visit = request.user.patientprofile.get_active_visit()

        if payment_amount != 0:
            # submit payment
            nonce = request.POST["payment_method_nonce"]
            result = braintree_submit_transaction(
                request.user, nonce, payment_amount)

            # check for errors
            if not result.is_success:
                for error in result.errors.deep_errors:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        'Your credit card payment was declined: %s (%s)' %
                        (error.message,
                         error.code))

                return redirect('patient-visit-review')

            # persist payment
            payment_record = PatientPaymentRecord(
                payment_amount=payment_amount,
                transaction_id=result.transaction.id)
        else:
            payment_record = PatientPaymentRecord(
                payment_amount=payment_amount)

        payment_record.save()
        payment_record.patientvisit = active_visit
        payment_record.save()

        active_visit.payment_record = payment_record
        active_visit.save()

        return redirect('patient-visit-status', visit_id=active_visit.id)

class Discount(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def post(self, request):
        context = RequestContext(request)

        discount_form = VisitDiscountCodeForm(request.POST)

        ERROR_STR = 'Sorry, that discount code is invalid.'
        if not discount_form.is_valid():
            messages.add_message(request, messages.ERROR, ERROR_STR)

            return redirect('patient-visit-review')

        try:
            code = discount_form.cleaned_data['code']
            c = VisitDiscount.objects.get(code=code)
        except ObjectDoesNotExist:
            messages.add_message(request, messages.ERROR, ERROR_STR)

            return redirect('patient-visit-review')

        now_time = timezone.make_aware(datetime.datetime.now(),
                timezone.get_default_timezone())
        if not c.is_active or \
            not (c.start <= now_time <= c.end):
            messages.add_message(request, messages.ERROR, ERROR_STR)

            return redirect('patient-visit-review')

        active_visit = request.user.patientprofile.get_active_visit()


        logger.info("Applying discount_code='%s' to visit." % (c.code,))

        messages.add_message(request, messages.SUCCESS,
            'Discount code applied.')
        active_visit.discount = c
        active_visit.save()

        return redirect('patient-visit-review')

class Status(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_specific_test_func
    raise_exception = True

    def get(self, request, visit_id):
        context = RequestContext(request)

        visit = PatientVisit.objects.get(pk=visit_id)

        context.update({
            'visit_id': visit_id,
            'date': visit.created,
            'amount': visit.payment_record.payment_amount,
            'transaction_id': visit.payment_record.transaction_id
        })

        return render(request, 'auth/patient/visit/finalized.html', context)

class Pharmacy(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = patient_visit_test_func
    raise_exception = True

    def post(self, request):
        form = PatientProfileSettingsForm(request.POST)
        user = request.user

        if form.is_valid():
            profile = user.patientprofile
            profile.preferred_pharmacy_id = form.cleaned_data['preferred_pharmacy_id']
            profile.save()

        if request.is_ajax():
            return JsonResponse({'success': True})
        return redirect('patient-visit-review')
