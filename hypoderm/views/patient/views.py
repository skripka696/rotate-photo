import json

from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import View
from braces.views import LoginRequiredMixin, UserPassesTestMixin
from django.conf import settings
from django.http import JsonResponse

from hypoderm.lib.utils import full_registration_required
from hypoderm.lib.dosespot import (
    perform_pharmacy_search, 
    DoseSpotException, 
    validate_pharmacy_id
)
from hypoderm.forms import PharmacySearchForm, PatientProfileSettingsForm

from logging import getLogger

logger = getLogger(__name__)

def user_is_patient_test_func(self, user):
    return user.groups.filter(name='patients').exists()

@login_required
@full_registration_required
@permission_required("hypoderm.view_patient_portal", raise_exception=True)
def home(request):
    context = RequestContext(request)

    active_visit = request.user.patientprofile.get_active_visit()
    context.push({'active_visit': active_visit})

    return render(request, 'auth/patient/index.html', context)
    
class PatientProfileSettings(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = user_is_patient_test_func
    
    def get(self, request):
        context = RequestContext(request)
        user = request.user
        email_preferences = user.emailpreferences
        profile = user.patientprofile
        form = PatientProfileSettingsForm(initial={'email':user.email, 'receive_newsletter':email_preferences.receive_newsletter, 'preferred_pharmacy_id':profile.preferred_pharmacy_id})
        
        context.push({'settings_form':form})

        return render(request, 'auth/common/settings.html', context)
        
    def post(self, request):
        context = RequestContext(request)
        form = PatientProfileSettingsForm(request.POST)
        user = request.user
        
        if form.is_valid():
            profile = user.patientprofile
            email_preferences = user.emailpreferences
            
            user.email = form.cleaned_data['email']
            user.save()
            
            email_preferences.receive_newsletter = form.cleaned_data['receive_newsletter']
            email_preferences.save()
            
            profile.email = form.cleaned_data['email']
            profile.preferred_pharmacy_id = form.cleaned_data['preferred_pharmacy_id']
            profile.save()

            return redirect('patient-profile-settings')
        else:
            context.push({'settings_form':form})
            return render(request, 'auth/common/settings.html', context)    

class PharmacySearchControllerView(LoginRequiredMixin, UserPassesTestMixin, View):
    test_func = user_is_patient_test_func
        
    def get(self, request):
        context = RequestContext(request)
        
        if 'zipcode' not in request.GET:
            form = PharmacySearchForm()
            
            context.push({
                'pharmacy_search_form':form,
            })
            
            return render(request, 'auth/patient/search.html', context)
            
        form = PharmacySearchForm(request.GET)
        
        if form.is_valid():
            logger.debug("Pharmacy search zipcode: %s" % form.cleaned_data['zipcode'])
        
            try:
                pharmacies = perform_pharmacy_search(form.cleaned_data['zipcode'])
            except DoseSpotException:
                form.add_error(None, 'Error contacting search host, please wait a few minutes and try your search again. If this error persists, please contact the site administrator.')
            
                context.push({
                    'pharmacy_search_form':form,
                })
            
                return render(request, 'auth/patient/search.html', context)
        
            return JsonResponse(pharmacies, safe=False)

        context.push({
            'pharmacy_search_form':form,
        })
    
        return render(request, 'auth/patient/search.html', context)