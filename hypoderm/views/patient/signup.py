import logging

from django.template import RequestContext
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User, Group
from django.views.generic import View
from braces.views import LoginRequiredMixin, MultiplePermissionsRequiredMixin

from hypoderm import signals

from hypoderm.forms import (
    PartialPatientSignupForm,
    PatientProfileForm,
    SecurityQuestionForm,
)

from hypoderm.models import (
    EmailPreferences,
    PatientProfile,
    Contract,
    PatientConsentRecord
)

logger = logging.getLogger(__name__)

class Parital(View):

    def get(self, request):
        if request.user.is_authenticated():
            return redirect('home')

        context = RequestContext(request)

        form = PartialPatientSignupForm()
        context.push({'form':form})

        return render(request, 'unauth/signup/patient/partial.html', context)

    def post(self, request):
        context = RequestContext(request)

        form = PartialPatientSignupForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                form.cleaned_data['username'],
                form.cleaned_data['email'],
                form.cleaned_data['password']
            )

            patient_group = Group.objects.get(name="patients")
            user.groups.add(patient_group)
            user.save()
            signals.patient_created.send(sender=self.__class__, user=user)

            ep = EmailPreferences(
                user=user,
                receive_newsletter=form.cleaned_data['newsletter_signup']
            )
            ep.save()

            user = authenticate(username=user.username,
                                password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('patient-setup')
            else:
                # we should never be here
                logger.error('Failed to create user')
        else:
            context.push({'form':form})

            return render(request, 'unauth/signup/patient/partial.html', context)



class Complete(LoginRequiredMixin, MultiplePermissionsRequiredMixin, View):
    raise_exception = True

    permissions = {
        'any': ["hypoderm.view_patient_portal"]
    }

    def get(self, request):
        try:
            profile = getattr(request.user, 'patientprofile')
            logger.warn('User fully registered; redirecting to home page')
            return redirect('home')
        except PatientProfile.DoesNotExist:
            pass

        contract = Contract.objects.filter(role='patient', is_active=True).latest('created')

        context = RequestContext(request)

        context.push({
            'security_question_form': SecurityQuestionForm(),
            'profile_form': PatientProfileForm(initial={'contract': contract.pk}),
        })

        return render(request, 'auth/setup/index.html', context)

    def post(self, request):
        context = RequestContext(request)

        profile_form = PatientProfileForm(request.POST)

        if profile_form.is_valid():
            patient_profile = profile_form.save(commit=False)
            patient_profile.user = request.user
            patient_profile.email = request.user.email
            patient_profile.save()

            contract_id = profile_form.cleaned_data.get('contract')
            if contract_id:
                contract = Contract.objects.get(pk=contract_id)
                PatientConsentRecord(patient=patient_profile, contract=contract).save()

            return redirect('patient-home')
        else:
            context.push({'profile_form':profile_form})

            return render(request, 'auth/setup/index.html', context)

