from django import template

register = template.Library()


@register.filter
def selected_choice(form, field_name):
    return dict(form.fields[field_name].choices)[form.initial[field_name]]
