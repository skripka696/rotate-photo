from django import forms
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from localflavor.us.forms import USZipCodeField
from logging import getLogger

from hypoderm.lib.dosespot import validate_pharmacy_id, DoseSpotException
from hypoderm.lib.widgets import PreferredPharmacyWidget
from .models import (
    SecurityQuestion,
    ProviderProfile,
    PatientProfile,
    PatientMedicalRecord,
    PatientConditionDescription,
    Photo,
    VisitComment,
    BodyPart,
)

logger = getLogger(__name__)

class PartialProviderSignupForm(forms.Form):
    username = forms.CharField(min_length=8)
    email = forms.EmailField()
    password = forms.CharField(min_length=8,
                               widget=forms.PasswordInput())
    newsletter_signup = forms.BooleanField(required=False)

    contract_agree = forms.BooleanField(required=True)
    contract = forms.CharField(required=False)

    def clean_email(self):
        email = self.cleaned_data['email'].lower().strip()

        existing_user = User.objects.filter(email__iexact=email)
        if existing_user:
            raise forms.ValidationError("Email already taken.")

        return email

    def clean_username(self):
        username = self.cleaned_data['username'].lower().strip()

        existing_user = User.objects.filter(username__iexact=username)
        if existing_user:
            raise forms.ValidationError("Username already taken.")

        return username

class PartialPatientSignupForm(forms.Form):
    username = forms.CharField(min_length=8)
    email = forms.EmailField()
    password = forms.CharField(min_length=8,
                               widget=forms.PasswordInput())
    newsletter_signup = forms.BooleanField(required=False)

    def clean_email(self):
        email = self.cleaned_data['email'].lower().strip()

        existing_user = User.objects.filter(email__iexact=email)
        if existing_user:
            raise forms.ValidationError("Email already taken.")

        return email

    def clean_username(self):
        username = self.cleaned_data['username'].lower().strip()

        existing_user = User.objects.filter(username__iexact=username)
        if existing_user:
            raise forms.ValidationError("Username already taken.")

        return username


class SecurityQuestionForm(forms.Form):
    security_question = forms.ModelChoiceField(
        queryset=SecurityQuestion.objects.filter(is_active=True))
    security_question_response = forms.CharField()


class PatientProfileForm(forms.ModelForm):
    required_css_class = 'required'

    contract_agree = forms.BooleanField(required=True)
    contract = forms.CharField(required=False, show_hidden_initial=True)

    def __init__(self, *args, **kwargs):
        super(PatientProfileForm, self).__init__(*args, **kwargs)

        self.fields['phone'].widget.attrs.update({
            'data-fv-phone': 'true',
            'data-fv-phone-country': 'US',
            'data-fv-phone-message': 'You must enter a valid phone number.',
            'data-fv-callback': 'true',
            'data-fv-callback-message': 'You must enter a valid area code.',
            'data-fv-callback-callback': 'checkAreaCode',
        })

        self.fields['zipcode'].widget.attrs.update({
            'data-fv-zipcode': 'true',
            'data-fv-zipcode-country': 'US',
            'data-fv-zipcode-message': 'You must enter a valid zipcode.',
        })

    class Meta:
        model = PatientProfile
        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'phone',
            'mail_one',
            'mail_two',
            'city',
            'state',
            'zipcode',
            'gender',
            'birth_date',
            'contract_agree',
            'contract',
        ]
        labels = {
            'mail_one': 'Address Line 1',
            'mail_two': 'Address Line 2',
        }


class PatientMedicalRecordForm(forms.ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(PatientMedicalRecordForm, self).__init__(*args, **kwargs)

    class Meta:
        model = PatientMedicalRecord
        fields = [
            'has_allergies',
            'medicines_taken',
            'surgical_procedures',
            'receiving_treatments',
            'has_predisposition',
            'body_weight',
            'employment_status',
            'lifestyle_factors',
            'skin_type',
        ]
        labels = {
            'has_allergies': 'Do you have any known medication allergies?',
            'medicines_taken': 'Are you taking any over-the-counter or prescription medicine right now?',
            'surgical_procedures': 'Have you had any surgical procedures, for any condition, in the past?',
            'receiving_treatments': 'Are you currently receiving treatment for any illnesses?',
            'has_predisposition': 'Are there any known skin diseases or skin cancer that run in your family?',
            'body_weight': 'What is your estimated weight in pounds?',
            'employment_status': 'What is your current employment status?',
            'lifestyle_factors': 'Do you do any of the following?',
            'skin_type': 'How would you describe your skin?',
        }
        widgets = {
            'lifestyle_factors': forms.CheckboxSelectMultiple(),
        }


class PatientConditionDescriptionForm(forms.ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(PatientConditionDescriptionForm, self).__init__(*args, **kwargs)
        self.fields['affected_body_parts'].choices = \
            [(part.pk, part.name, part.polygon, part.type) for part in BodyPart.objects.all()]

    class Meta:
        model = PatientConditionDescription
        fields = [
            'description',
            'symptoms',
            'symptoms_severity',
            'condition_duration_days',
            'condition_duration_months',
            'condition_duration_years',
            'aggravator',
            'prior_treatment',
            'suspected_changes',
            'additional_information',
            'affected_body_parts',
        ]
        labels = {
            'description': 'Please provide a description of the skin, hair or nail condition you are experiencing.',
            'symptoms': 'Which of the following describes how the condition feels? Check only those that apply.',
            'symptoms_severity': 'How would you rate the severity of your symptoms for the reported skin, hair or nail condition?',
            'condition_duration_days': 'Days',
            'condition_duration_months': 'Months',
            'condition_duration_years': 'Years',
            'aggravator': 'Can you identify anything that makes the condition worse?',
            'prior_treatment': 'Have you done anything, either independently or with another health care provider, to help the condition?',
            'suspected_changes': 'Can you link this problem to a medication or dosage change, the use of new cosmetics, toiletries (e.g., soaps, detergents or shampoos), an occupational exposure, or a recreational activity?',
            'additional_information': 'Do you have any additional details about your condition?',
            'affected_body_parts': 'Please select all the areas where you\'re experiencing this problem.',
        }
        widgets = {
            'symptoms': forms.CheckboxSelectMultiple(attrs={'class': 'symptom-field'}),
            'affected_body_parts': forms.CheckboxSelectMultiple(),
            'condition_duration_days': forms.NumberInput(attrs={'class': 'durationField', 'min': 0}),
            'condition_duration_months': forms.NumberInput(attrs={'class': 'durationField', 'min': 0}),
            'condition_duration_years': forms.NumberInput(attrs={'class': 'durationField', 'min': 0}),
        }


class PhotoForm(forms.ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(PhotoForm, self).__init__(*args, **kwargs)

        self.fields['photo_data'].widget.attrs.update({
            'data-fv-file': 'true',
            'data-fv-file-type': 'image/jpeg,image/png',
            'data-fv-file-message': 'The selected file is not valid.',
            'required': 'required',
        })

    class Meta:
        model = Photo
        fields = [
            'photo_data',
            'body_part',
            'description',
        ]
        labels = {
            'photo_data': 'Photo'
        }
        widgets = {
            'body_part': forms.HiddenInput(),
            'description': forms.Textarea(attrs={
                'rows': 1,
                'placeholder': 'Please describe what\'s wrong.',
            }),
        }

class ReferralFaxForm(forms.Form):
    required_css_class = 'required'

    destination_name = forms.CharField(label='Name', required=False)
    destination = forms.CharField(
        label='Fax Number',
        widget=forms.TextInput(attrs={
            'data-fv-phone': 'true',
            'data-fv-phone-country': 'US',
            'data-fv-phone-message': 'You must enter a valid phone number.',
        })
    )

    specialty = forms.CharField(label='Specialty', required=False)
    phone = forms.CharField(label='Phone Number', required=False)

    text = forms.CharField(
        label='Referral Message',
        widget=forms.Textarea(attrs={
            'rows': 2,
        }))

class VisitCommentForm(forms.ModelForm):
    class Meta:
        model = VisitComment
        fields = [
            'text',
        ]

class VisitDiscountCodeForm(forms.Form):
    required_css_class = 'required'

    code = forms.CharField(label='Discount Code')

class ProviderProfileSettingsForm(forms.Form):
    email = forms.CharField(label='Change Email Address', required=False)
    receive_newsletter = forms.BooleanField(label='Receive Newsletter', required=False)

class PatientProfileSettingsForm(forms.Form):
    email = forms.CharField(label='Change Email Address', required=False)
    receive_newsletter = forms.BooleanField(label='Receive Newsletter', required=False)
    preferred_pharmacy_id = forms.IntegerField(widget=PreferredPharmacyWidget, label='Preferred Pharmacy ID', required=False)
    
    def clean_preferred_pharmacy_id(self):
        try:
            validate_pharmacy_id(self.cleaned_data['preferred_pharmacy_id'])
        except DoseSpotException as e:
            logger.debug(str(e))
            raise forms.ValidationError(e.message)
        return self.cleaned_data['preferred_pharmacy_id']
    
class PharmacySearchForm(forms.Form):
    zipcode = USZipCodeField()
