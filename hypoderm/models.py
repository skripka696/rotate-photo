from django.utils import timezone

from django.db import models
from django.contrib.auth.models import User

from localflavor.us.models import *

# Create your models here.


class TimeStampedModel(models.Model):
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(TimeStampedModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class BodyPart(models.Model):
    BODY_PART_SIDE_CHOICES = (
        (0, 'Front'),
        (1, 'Back'),
    )

    name = models.CharField(max_length=256)
    polygon = models.TextField(default='')
    type = models.IntegerField(default=0, choices=BODY_PART_SIDE_CHOICES)

    def __str__(self):
        return '%s' % (self.name)


class Contract(TimeStampedModel):
    contract_text = models.TextField()

    role = models.CharField(max_length=16)

    is_active = models.BooleanField()

    def __str__(self):
        return "{0} (role='{1}')".format(type(self).__name__, self.role)


class SecurityQuestion(TimeStampedModel):
    question = models.CharField(max_length=256)

    is_active = models.BooleanField()


class EmailPreferences(TimeStampedModel):
    user = models.OneToOneField(User)
    receive_newsletter = models.BooleanField()


class PatientProfile(TimeStampedModel):
    user = models.OneToOneField(User)

    first_name = models.CharField(max_length=32)
    middle_name = models.CharField(max_length=32, blank=True)
    last_name = models.CharField(max_length=32)

    email = models.EmailField()

    phone = models.CharField(max_length=16)

    mail_one = models.CharField(max_length=128)
    mail_two = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=32)
    state = USStateField()
    zipcode = USZipCodeField()

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    birth_date = models.DateField()

    security_questions = models.ManyToManyField(
        SecurityQuestion,
        through='PatientSecurityQuestionResponse',
        through_fields=(
            'patient',
            'security_question'))
    contracts = models.ManyToManyField(
        Contract, through='PatientConsentRecord')

    dosespot_id = models.IntegerField(blank=True, null=True)
    preferred_pharmacy_id = models.IntegerField(blank=True, null=True)

    def get_active_visit(self):
        try:
            return PatientVisit.objects.get(patient=self,
                                            is_active=True, is_deleted=False)
        except PatientVisit.DoesNotExist:
            return None

    def __str__(self):
        return "{0} (username='{1}')".format(type(self).__name__, self.user)


class PatientSecurityQuestionResponse(TimeStampedModel):
    patient = models.ForeignKey(PatientProfile)
    security_question = models.ForeignKey(SecurityQuestion)
    answer = models.CharField(max_length=128)  # this is hashed


class ProviderProfile(TimeStampedModel):
    user = models.OneToOneField(User)
    email = models.EmailField()
    clinician_id = models.IntegerField(blank=True, null=True)

    contracts = models.ManyToManyField(
        Contract, through='ProviderConsentRecord')

    def __str__(self):
        return "{0} (username='{1}')".format(type(self).__name__, self.user)


class PatientConsentRecord(TimeStampedModel):
    patient = models.ForeignKey(PatientProfile)
    contract = models.ForeignKey(Contract)


class ProviderConsentRecord(TimeStampedModel):
    provider = models.ForeignKey(ProviderProfile)
    contract = models.ForeignKey(Contract)


class LifestyleChoice(TimeStampedModel):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return '%s' % (self.name)


class Symptom(TimeStampedModel):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return '%s' % (self.name)


class PatientMedicalRecord(TimeStampedModel):
    patient = models.ForeignKey(PatientProfile)

    has_allergies = models.TextField(blank=True)
    medicines_taken = models.TextField(blank=True)
    surgical_procedures = models.TextField(blank=True)
    receiving_treatments = models.TextField(blank=True)
    has_predisposition = models.TextField(blank=True)
    body_weight = models.PositiveSmallIntegerField()

    EMPLOYMENT_STATUS_CHOICES = (
        ('S', 'Student'),
        ('E', 'Employed'),
        ('U', 'Unemployed'),
        ('R', 'Retired'),
    )
    employment_status = models.CharField(
        max_length=1,
        choices=EMPLOYMENT_STATUS_CHOICES)

    lifestyle_factors = models.ManyToManyField(LifestyleChoice, blank=True)

    SKIN_TYPE_CHOICES = (
        ('N', 'Normal'),
        ('O', 'Oily'),
        ('D', 'Dry'),
        ('S', 'Sensitive'),
        ('D', 'Combination'),
    )

    skin_type = models.CharField(
        max_length=1,
        choices=SKIN_TYPE_CHOICES)


class PatientPaymentRecord(TimeStampedModel):
    payment_amount = models.DecimalField(max_digits=5, decimal_places=2)
    transaction_id = models.CharField(max_length=128, null=True, blank=True)


class FaxTransactionReceipt(TimeStampedModel):
    raw_return_payload = models.TextField()


class PatientVisit(TimeStampedModel):

    class Meta:
        # patients should only have one active Visit
        unique_together = (('is_active', 'patient'))

    def is_confirmed(self):
        try:
            return self.payment_record
        except PatientPaymentRecord.DoesNotExist:
            return None
    patient = models.ForeignKey(PatientProfile)
    medical_record = models.OneToOneField(PatientMedicalRecord)

    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    payment_record = models.OneToOneField(PatientPaymentRecord,
            null=True,
            blank=True)
    discount = models.ForeignKey('VisitDiscount', null=True, blank=True)

    def get_status(self):
        if not self.is_active:
            return 'inactive'
        if self.is_deleted:
            return 'deleted'
        if self.is_confirmed():
            return 'confirmed'

        condition_descriptions = \
            PatientConditionDescription.objects.filter(patient_visit=self)

        if not condition_descriptions:
            return 'record_captured'

            # patient must have at least one photo for each condition attached
            # to the patient visit.
        photos = \
            Photo.objects.filter(
                condition_description__in=condition_descriptions)

        if not photos:
            return 'conditions_captured'

        return 'pending_confirmation'

    diagnoses = models.ManyToManyField('Diagnosis', blank=True,
            through='PatientDiagnosis')
    treatments = models.ManyToManyField('Treatment', blank=True,
            through='PatientTreatment')
    educations = models.ManyToManyField('Education',
            blank=True, through='PatientEducation')

    def __str__(self):
        return "{0} (username='{1}', visit_id='{2}')".format(
            type(self).__name__, self.patient.user, self.patient.id)


class FaxReferral(TimeStampedModel):
    requestor = models.ForeignKey(User)
    visit = models.ForeignKey(PatientVisit, null=True)

    destination = models.CharField(max_length=16)
    destination_name = models.CharField(max_length=64)
    specialty = models.CharField(max_length=64, null=True, blank=True)
    phone = models.CharField(max_length=16, null=True, blank=True)
    text = models.TextField()

    transaction_id = models.CharField(max_length=64, null=True, blank=True)
    http_status_code = models.IntegerField(null=True, blank=True)
    error_json = models.TextField(null=True, blank=True)

    receipt = models.OneToOneField(FaxTransactionReceipt, null=True, blank=True)


class PatientConditionDescription(TimeStampedModel):
    patient_visit = models.ForeignKey(PatientVisit)

    description = models.TextField()
    symptoms = models.ManyToManyField(Symptom, blank=True)

    SEVERITY_CHOICES = (
        (0, '0 - No Symptoms'),
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
        (7, '7'),
        (8, '8'),
        (9, '9'),
        (10, '10 - Severe Symptoms'),
    )
    symptoms_severity = models.IntegerField(choices=SEVERITY_CHOICES)

    condition_duration_days = models.IntegerField(default=0)
    condition_duration_months = models.IntegerField(default=0)
    condition_duration_years = models.IntegerField(default=0)

    aggravator = models.TextField(blank=True)
    prior_treatment = models.TextField(blank=True)
    suspected_changes = models.TextField(blank=True)
    additional_information = models.TextField(blank=True)

    affected_body_parts = models.ManyToManyField(BodyPart)


class Photo(TimeStampedModel):
    body_part = models.ForeignKey(BodyPart)
    condition_description = models.ForeignKey(PatientConditionDescription)
    photo_data = models.ImageField(upload_to='patient_imgs')
    description = models.TextField()

    def __str__(self):
        return "{0} (username='{1}', body_part.name='{2}', visit_id='{3}')".format(
            type(self).__name__,
            self.condition_description.patient_visit.patient.user,
            self.body_part.name,
            self.condition_description.patient_visit.id)


class VisitComment(TimeStampedModel):
    user = models.ForeignKey(User)
    visit = models.ForeignKey(PatientVisit)

    text = models.TextField()
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return "{0} (username='{1}', visit_id='{2}')".format(
            type(self).__name__,
            self.user, self.visit.patient.id)


class Notification(TimeStampedModel):
    user = models.ForeignKey(User)
    message = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    link = models.URLField(null=True, blank=False, max_length=800)

    def __str__(self):
        return "{0} (username='{1}', message='{2}', is_active='{3}')".format(
            type(self).__name__, self.user,
            self.message,
            self.is_active)


class Diagnosis(TimeStampedModel):
    name = models.CharField(max_length=256, unique=True)

    educations = models.ManyToManyField('Education')

    def __str__(self):
        return '%s' % (self.name)


class PatientDiagnosis(TimeStampedModel):
    diagnosis = models.ForeignKey(Diagnosis)
    visit = models.ForeignKey(PatientVisit)

    class Meta:
        unique_together = (('diagnosis', 'visit'))


class Treatment(TimeStampedModel):
    name = models.CharField(max_length=256, unique=True)

    def __str__(self):
        return '%s' % (self.name)


class PatientTreatment(TimeStampedModel):
    treatment = models.ForeignKey(Treatment)
    visit = models.ForeignKey(PatientVisit)

    class Meta:
        unique_together = (('treatment', 'visit'))


class DiagnosisTreatment(TimeStampedModel):
    rank = models.PositiveSmallIntegerField()
    treatment = models.ForeignKey(Treatment)
    diagnosis = models.ForeignKey(Diagnosis)

    class Meta:
        unique_together = (('rank', 'treatment', 'diagnosis'))


class Education(TimeStampedModel):
    title = models.CharField(max_length=256, unique=True)
    text = models.TextField()

    def __str__(self):
        return '%s' % (self.title)


class PatientEducation(TimeStampedModel):
    education = models.ForeignKey(Education)
    visit = models.ForeignKey(PatientVisit)

    class Meta:
        unique_together = (('education', 'visit'))


class VisitDiscount(TimeStampedModel):
    code = models.CharField(max_length=16, unique=True)
    discount_amount = models.DecimalField(max_digits=5, decimal_places=2)
    is_active = models.BooleanField(default=False)

    start = models.DateTimeField()
    end = models.DateTimeField()

    def __str__(self):
        return '%s' % (self.code)
