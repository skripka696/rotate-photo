from django.contrib import admin

from .models import (
    SecurityQuestion,
    PatientProfile,
    PatientVisit,
    PatientMedicalRecord,
    PatientConditionDescription,
    Photo,
    VisitComment,
    BodyPart,
    Symptom,
    Notification,
    Treatment,
    PatientTreatment,
    Diagnosis,
    PatientDiagnosis,
    Education,
    PatientEducation,
    DiagnosisTreatment,
    Contract,
    PatientConsentRecord,
    ProviderProfile,
    ProviderConsentRecord,
    VisitDiscount,
)


class PatientContractInline(admin.TabularInline):
    model = PatientConsentRecord
    extra = 1


class ProviderContractInline(admin.TabularInline):
    model = ProviderConsentRecord
    extra = 1


class PatientProfileAdmin(admin.ModelAdmin):
    inlines = (PatientContractInline,)


class ProviderProfileAdmin(admin.ModelAdmin):
    inlines = (ProviderContractInline,)


admin.site.register(SecurityQuestion)
admin.site.register(PatientProfile, PatientProfileAdmin)
admin.site.register(PatientVisit)
admin.site.register(PatientMedicalRecord)
admin.site.register(PatientConditionDescription)
admin.site.register(Photo)
admin.site.register(VisitComment)
admin.site.register(BodyPart)
admin.site.register(Symptom)
admin.site.register(Notification)
admin.site.register(Treatment)
admin.site.register(PatientTreatment)
admin.site.register(Diagnosis)
admin.site.register(PatientDiagnosis)
admin.site.register(Education)
admin.site.register(PatientEducation)
admin.site.register(DiagnosisTreatment)
admin.site.register(Contract)
admin.site.register(ProviderProfile, ProviderProfileAdmin)
admin.site.register(VisitDiscount)
