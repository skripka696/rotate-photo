# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0019_patientprofile_preferred_pharmacy_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='VisitDiscount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('code', models.CharField(unique=True, max_length=16)),
                ('discount_amount', models.DecimalField(max_digits=5, decimal_places=2)),
                ('is_active', models.BooleanField(default=False)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='patientpaymentrecord',
            name='discount',
            field=models.ForeignKey(blank=True, to='hypoderm.VisitDiscount', null=True),
        ),
    ]
