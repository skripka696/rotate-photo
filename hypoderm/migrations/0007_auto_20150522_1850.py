# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0006_auto_20150521_2320'),
    ]

    operations = [
        migrations.AddField(
            model_name='faxreferral',
            name='destination_name',
            field=models.CharField(default='foobar', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='phone',
            field=models.CharField(max_length=16, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='specialty',
            field=models.CharField(max_length=64, null=True, blank=True),
        ),
    ]
