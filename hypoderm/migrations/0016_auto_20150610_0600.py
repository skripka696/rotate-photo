# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0015_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='providerprofile',
            name='contracts',
            field=models.ManyToManyField(to='hypoderm.Contract', through='hypoderm.ProviderConsentRecord'),
        ),
    ]
