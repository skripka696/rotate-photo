# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hypoderm', '0007_auto_20150522_1850'),
    ]

    operations = [
        migrations.CreateModel(
            name='VisitComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('text', models.TextField()),
                ('is_deleted', models.BooleanField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='comment',
            name='user',
        ),
        migrations.RemoveField(
            model_name='photocomment',
            name='comment',
        ),
        migrations.RemoveField(
            model_name='photocomment',
            name='photo',
        ),
        migrations.RemoveField(
            model_name='patientvisit',
            name='referrals',
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='visit',
            field=models.ForeignKey(to='hypoderm.PatientVisit', null=True),
        ),
        migrations.AlterField(
            model_name='patientvisit',
            name='payment_record',
            field=models.OneToOneField(null=True, blank=True, to='hypoderm.PatientPaymentRecord'),
        ),
        migrations.DeleteModel(
            name='Comment',
        ),
        migrations.DeleteModel(
            name='PhotoComment',
        ),
        migrations.AddField(
            model_name='visitcomment',
            name='visit',
            field=models.ForeignKey(to='hypoderm.PatientVisit'),
        ),
    ]
