# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0005_auto_20150521_2248'),
    ]

    operations = [
        migrations.RenameField(
            model_name='faxreferral',
            old_name='http_return_code',
            new_name='http_status_code',
        ),
    ]
