# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0012_providerprofile_email'),
        ('hypoderm', '0011_add_groups'),
    ]

    operations = [
    ]
