# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0017_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='providerprofile',
            name='clinician_id',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
