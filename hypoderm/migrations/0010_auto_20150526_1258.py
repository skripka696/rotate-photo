# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0009_auto_20150525_2155'),
    ]

    operations = [
        migrations.AddField(
            model_name='bodypart',
            name='polygon',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='bodypart',
            name='type',
            field=models.IntegerField(default=0, choices=[(0, b'Front'), (1, b'Back')]),
        ),
    ]
