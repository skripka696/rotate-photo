# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0011_providerconsentrecord'),
        ('hypoderm', '0014_auto_20150608_0700'),
    ]

    operations = [
    ]
