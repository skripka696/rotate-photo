# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0010_auto_20150526_1258'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProviderConsentRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('contract', models.ForeignKey(to='hypoderm.Contract')),
                ('provider', models.ForeignKey(to='hypoderm.ProviderProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
