# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def make_permissions(apps, schema_editor):
    """Create permissions and groups for role-based access control for patients and providers.
    
    Patient view permission is attached to the patient profile model
    Provider view permission is attached to the provider profile model
    
    See https://github.com/cornmander/derm/issues/14
    """
    Group = apps.get_model("auth","Group")
    Permission = apps.get_model("auth","Permission")
    ContentType = apps.get_model("contenttypes", "ContentType")
    PatientVisit = apps.get_model("hypoderm", "PatientVisit")
    PatientProfile = apps.get_model("hypoderm", "PatientProfile")
    ProviderProfile = apps.get_model("hypoderm", "ProviderProfile")
    VisitComment = apps.get_model("hypoderm", "VisitComment")
    
    patient_visit_content = ContentType.objects.get_for_model(PatientVisit)
    patient_visit_permission, _ = Permission.objects.get_or_create(name="Can view patient visit", codename="view_patient_visit", content_type=patient_visit_content)
    administer_visit_permission, _ = Permission.objects.get_or_create(name="Can administer patient visit", codename="administer_patient_visit", content_type=patient_visit_content)
    
    patient_portal_content = ContentType.objects.get_for_model(PatientProfile)
    patient_view_permission, _ = Permission.objects.get_or_create(name="Can view patient portal", codename="view_patient_portal", content_type=patient_portal_content)
    
    provider_portal_content = ContentType.objects.get_for_model(ProviderProfile)
    provider_view_permission, _ = Permission.objects.get_or_create(name="Can view provider portal", codename="view_provider_portal", content_type=provider_portal_content)
    
    visit_comment_content = ContentType.objects.get_for_model(VisitComment)
    add_comment_permission, _ = Permission.objects.get_or_create(name="Can add visit comment", codename="add_visitcomment", content_type=visit_comment_content)
    
    patients, _ = Group.objects.get_or_create(name="patients")
    providers, _ = Group.objects.get_or_create(name="providers")
    
    patients.permissions.add(patient_visit_permission, patient_view_permission, add_comment_permission)
    providers.permissions.add(patient_visit_permission, provider_view_permission, add_comment_permission, administer_visit_permission)
    
class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0010_auto_20150526_1258'),
    ]

    operations = [
        migrations.RunPython(make_permissions,reverse_code=lambda *args,**kwargs: True),
    ]