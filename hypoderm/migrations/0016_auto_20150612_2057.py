# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0015_auto_20150612_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='link',
            field=models.URLField(max_length=800, null=True),
        ),
    ]
