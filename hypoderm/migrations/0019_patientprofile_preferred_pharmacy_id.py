# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0018_providerprofile_clinician_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='patientprofile',
            name='preferred_pharmacy_id',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
