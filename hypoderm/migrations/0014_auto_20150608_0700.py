# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0013_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='Diagnosis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(unique=True, max_length=256)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DiagnosisTreatment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('rank', models.PositiveSmallIntegerField()),
                ('diagnosis', models.ForeignKey(to='hypoderm.Diagnosis')),
            ],
        ),
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('title', models.CharField(unique=True, max_length=256)),
                ('text', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientDiagnosis',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('diagnosis', models.ForeignKey(to='hypoderm.Diagnosis')),
                ('visit', models.ForeignKey(to='hypoderm.PatientVisit')),
            ],
        ),
        migrations.CreateModel(
            name='PatientEducation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('education', models.ForeignKey(to='hypoderm.Education')),
                ('visit', models.ForeignKey(to='hypoderm.PatientVisit')),
            ],
        ),
        migrations.CreateModel(
            name='PatientTreatment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Treatment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(unique=True, max_length=256)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='patienttreatment',
            name='treatment',
            field=models.ForeignKey(to='hypoderm.Treatment'),
        ),
        migrations.AddField(
            model_name='patienttreatment',
            name='visit',
            field=models.ForeignKey(to='hypoderm.PatientVisit'),
        ),
        migrations.AddField(
            model_name='diagnosistreatment',
            name='treatment',
            field=models.ForeignKey(to='hypoderm.Treatment'),
        ),
        migrations.AddField(
            model_name='diagnosis',
            name='educations',
            field=models.ManyToManyField(to='hypoderm.Education'),
        ),
        migrations.AddField(
            model_name='patientvisit',
            name='diagnoses',
            field=models.ManyToManyField(to='hypoderm.Diagnosis', null=True, through='hypoderm.PatientDiagnosis', blank=True),
        ),
        migrations.AddField(
            model_name='patientvisit',
            name='educations',
            field=models.ManyToManyField(to='hypoderm.Education', null=True, through='hypoderm.PatientEducation', blank=True),
        ),
        migrations.AddField(
            model_name='patientvisit',
            name='treatments',
            field=models.ManyToManyField(to='hypoderm.Treatment', null=True, through='hypoderm.PatientTreatment', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='patienttreatment',
            unique_together=set([('treatment', 'visit')]),
        ),
        migrations.AlterUniqueTogether(
            name='patienteducation',
            unique_together=set([('education', 'visit')]),
        ),
        migrations.AlterUniqueTogether(
            name='patientdiagnosis',
            unique_together=set([('diagnosis', 'visit')]),
        ),
        migrations.AlterUniqueTogether(
            name='diagnosistreatment',
            unique_together=set([('rank', 'treatment', 'diagnosis')]),
        ),
    ]
