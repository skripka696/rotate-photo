# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0002_auto_20150506_1235'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaxReferral',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('location', models.CharField(max_length=200, null=True)),
                ('destination', models.CharField(max_length=16)),
                ('text', models.TextField()),
                ('status', models.IntegerField(null=True)),
                ('error_json', models.TextField(null=True)),
                ('requestor', models.ForeignKey(to='hypoderm.ProviderProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='patientvisit',
            name='referrals',
            field=models.ManyToManyField(to='hypoderm.FaxReferral', blank=True),
        ),
    ]
