# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0014_auto_20150608_0700'),
    ]

    operations = [
        migrations.AddField(
            model_name='patientprofile',
            name='dosespot_id',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='patientvisit',
            name='diagnoses',
            field=models.ManyToManyField(to='hypoderm.Diagnosis', through='hypoderm.PatientDiagnosis', blank=True),
        ),
        migrations.AlterField(
            model_name='patientvisit',
            name='educations',
            field=models.ManyToManyField(to='hypoderm.Education', through='hypoderm.PatientEducation', blank=True),
        ),
        migrations.AlterField(
            model_name='patientvisit',
            name='treatments',
            field=models.ManyToManyField(to='hypoderm.Treatment', through='hypoderm.PatientTreatment', blank=True),
        ),
    ]
