# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0003_auto_20150520_2316'),
    ]

    operations = [
        migrations.CreateModel(
            name='FaxTransactionReceipt',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('raw_return_payload', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='faxreferral',
            name='location',
        ),
        migrations.RemoveField(
            model_name='faxreferral',
            name='status',
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='http_return_code',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='transaction_id',
            field=models.CharField(max_length=64, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='faxreferral',
            name='error_json',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='faxreferral',
            name='receipt',
            field=models.OneToOneField(null=True, blank=True, to='hypoderm.FaxTransactionReceipt'),
        ),
    ]
