# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0021_auto_20150620_0623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patientpaymentrecord',
            name='transaction_id',
            field=models.CharField(max_length=128, null=True, blank=True),
        ),
    ]
