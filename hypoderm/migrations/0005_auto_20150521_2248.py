# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0004_auto_20150521_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faxreferral',
            name='requestor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
