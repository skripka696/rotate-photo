# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import localflavor.us.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BodyPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('contract_text', models.TextField()),
                ('role', models.CharField(max_length=16)),
                ('is_active', models.BooleanField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EmailPreferences',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('receive_newsletter', models.BooleanField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='LifestyleChoice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(unique=True, max_length=64)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientConditionDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('description', models.TextField()),
                ('symptoms_severity', models.IntegerField(choices=[(0, b'0 - No Symptoms'), (1, b'1'), (2, b'2'), (3, b'3'), (4, b'4'), (5, b'5'), (6, b'6'), (7, b'7'), (8, b'8'), (9, b'9'), (10, b'10 - Severe Symptoms')])),
                ('condition_duration_days', models.IntegerField(default=0)),
                ('condition_duration_months', models.IntegerField(default=0)),
                ('condition_duration_years', models.IntegerField(default=0)),
                ('aggravator', models.TextField(blank=True)),
                ('prior_treatment', models.TextField(blank=True)),
                ('suspected_changes', models.TextField(blank=True)),
                ('additional_information', models.TextField(blank=True)),
                ('affected_body_parts', models.ManyToManyField(to='hypoderm.BodyPart')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientConsentRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('contract', models.ForeignKey(to='hypoderm.Contract')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientMedicalRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('has_allergies', models.TextField(blank=True)),
                ('medicines_taken', models.TextField(blank=True)),
                ('surgical_procedures', models.TextField(blank=True)),
                ('receiving_treatments', models.TextField(blank=True)),
                ('has_predisposition', models.TextField(blank=True)),
                ('body_weight', models.PositiveSmallIntegerField()),
                ('employment_status', models.CharField(max_length=1, choices=[(b'S', b'Student'), (b'E', b'Employed'), (b'U', b'Unemployed'), (b'R', b'Retired')])),
                ('skin_type', models.CharField(max_length=1, choices=[(b'N', b'Normal'), (b'O', b'Oily'), (b'D', b'Dry'), (b'S', b'Sensitive'), (b'D', b'Combination')])),
                ('lifestyle_factors', models.ManyToManyField(to='hypoderm.LifestyleChoice', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientPaymentRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('payment_amount', models.DecimalField(max_digits=5, decimal_places=2)),
                ('transaction_id', models.CharField(max_length=128)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('first_name', models.CharField(max_length=32)),
                ('middle_name', models.CharField(max_length=32, blank=True)),
                ('last_name', models.CharField(max_length=32)),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.CharField(max_length=16)),
                ('mail_one', models.CharField(max_length=128)),
                ('mail_two', models.CharField(max_length=128, blank=True)),
                ('city', models.CharField(max_length=32)),
                ('state', localflavor.us.models.USStateField(max_length=2, choices=[(b'AL', b'Alabama'), (b'AK', b'Alaska'), (b'AS', b'American Samoa'), (b'AZ', b'Arizona'), (b'AR', b'Arkansas'), (b'AA', b'Armed Forces Americas'), (b'AE', b'Armed Forces Europe'), (b'AP', b'Armed Forces Pacific'), (b'CA', b'California'), (b'CO', b'Colorado'), (b'CT', b'Connecticut'), (b'DE', b'Delaware'), (b'DC', b'District of Columbia'), (b'FL', b'Florida'), (b'GA', b'Georgia'), (b'GU', b'Guam'), (b'HI', b'Hawaii'), (b'ID', b'Idaho'), (b'IL', b'Illinois'), (b'IN', b'Indiana'), (b'IA', b'Iowa'), (b'KS', b'Kansas'), (b'KY', b'Kentucky'), (b'LA', b'Louisiana'), (b'ME', b'Maine'), (b'MD', b'Maryland'), (b'MA', b'Massachusetts'), (b'MI', b'Michigan'), (b'MN', b'Minnesota'), (b'MS', b'Mississippi'), (b'MO', b'Missouri'), (b'MT', b'Montana'), (b'NE', b'Nebraska'), (b'NV', b'Nevada'), (b'NH', b'New Hampshire'), (b'NJ', b'New Jersey'), (b'NM', b'New Mexico'), (b'NY', b'New York'), (b'NC', b'North Carolina'), (b'ND', b'North Dakota'), (b'MP', b'Northern Mariana Islands'), (b'OH', b'Ohio'), (b'OK', b'Oklahoma'), (b'OR', b'Oregon'), (b'PA', b'Pennsylvania'), (b'PR', b'Puerto Rico'), (b'RI', b'Rhode Island'), (b'SC', b'South Carolina'), (b'SD', b'South Dakota'), (b'TN', b'Tennessee'), (b'TX', b'Texas'), (b'UT', b'Utah'), (b'VT', b'Vermont'), (b'VI', b'Virgin Islands'), (b'VA', b'Virginia'), (b'WA', b'Washington'), (b'WV', b'West Virginia'), (b'WI', b'Wisconsin'), (b'WY', b'Wyoming')])),
                ('zipcode', localflavor.us.models.USZipCodeField(max_length=10)),
                ('gender', models.CharField(max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('birth_date', models.DateField()),
                ('contracts', models.ManyToManyField(to='hypoderm.Contract', through='hypoderm.PatientConsentRecord')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientSecurityQuestionResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('answer', models.CharField(max_length=128)),
                ('patient', models.ForeignKey(to='hypoderm.PatientProfile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PatientVisit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('is_active', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('medical_record', models.OneToOneField(to='hypoderm.PatientMedicalRecord')),
                ('patient', models.ForeignKey(to='hypoderm.PatientProfile')),
                ('payment_record', models.OneToOneField(null=True, to='hypoderm.PatientPaymentRecord')),
            ],
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('photo_data', models.ImageField(upload_to=b'patient_imgs')),
                ('body_part', models.ForeignKey(to='hypoderm.BodyPart')),
                ('condition_description', models.ForeignKey(to='hypoderm.PatientConditionDescription')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PhotoComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('text', models.CharField(max_length=1024)),
                ('photo', models.ForeignKey(to='hypoderm.Photo')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProviderProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='SecurityQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('question', models.CharField(max_length=256)),
                ('is_active', models.BooleanField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Symptom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(editable=False)),
                ('modified', models.DateTimeField()),
                ('name', models.CharField(unique=True, max_length=64)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='patientsecurityquestionresponse',
            name='security_question',
            field=models.ForeignKey(to='hypoderm.SecurityQuestion'),
        ),
        migrations.AddField(
            model_name='patientprofile',
            name='security_questions',
            field=models.ManyToManyField(to='hypoderm.SecurityQuestion', through='hypoderm.PatientSecurityQuestionResponse'),
        ),
        migrations.AddField(
            model_name='patientprofile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='patientmedicalrecord',
            name='patient',
            field=models.ForeignKey(to='hypoderm.PatientProfile'),
        ),
        migrations.AddField(
            model_name='patientconsentrecord',
            name='patient',
            field=models.ForeignKey(to='hypoderm.PatientProfile'),
        ),
        migrations.AddField(
            model_name='patientconditiondescription',
            name='patient_visit',
            field=models.ForeignKey(to='hypoderm.PatientVisit'),
        ),
        migrations.AddField(
            model_name='patientconditiondescription',
            name='symptoms',
            field=models.ManyToManyField(to='hypoderm.Symptom', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='patientvisit',
            unique_together=set([('is_active', 'patient')]),
        ),
    ]
