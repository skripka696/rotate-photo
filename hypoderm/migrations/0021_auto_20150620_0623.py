# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hypoderm', '0020_auto_20150620_0611'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='patientpaymentrecord',
            name='discount',
        ),
        migrations.AddField(
            model_name='patientvisit',
            name='discount',
            field=models.ForeignKey(blank=True, to='hypoderm.VisitDiscount', null=True),
        ),
    ]
