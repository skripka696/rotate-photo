from django.conf.urls import url, include

import hypoderm.views.unauth

import hypoderm.views.auth.views
import hypoderm.views.auth.visit

import hypoderm.views.patient.views
import hypoderm.views.patient.signup
import hypoderm.views.patient.visit

import hypoderm.views.provider.views
import hypoderm.views.provider.visit
import hypoderm.views.provider.signup

urlpatterns = [
    # unauth pages
    url(r'^$', hypoderm.views.unauth.views.index, name='landing-page'),
    url(r'^about-us/$',
        hypoderm.views.unauth.views.AboutUsView.as_view(),
        name='about-us'),
    url(r'^how-it-works/$',
        hypoderm.views.unauth.views.HowItWorksView.as_view(),
        name='how-it-works'),
    url(r'^quality-care/$',
        hypoderm.views.unauth.views.QualityCareView.as_view(),
        name='quality-care'),

    # contract page
    url(r'^contract/(?P<pk>[0-9]+)/$',
        hypoderm.views.unauth.views.ContractView.as_view(),
        name='contract'),


    # misc URLs
    url(r'^home/$', hypoderm.views.auth.views.home, name='home'),
    url(r'^callbacks/fax$',
        hypoderm.views.unauth.fax.Callback.as_view(),
        name='fax-callback'),

    # notification URLs
    url(r'^common/notifications$',
        hypoderm.views.auth.views.Notifications.as_view(),
        name='notifications'),
    url(r'^common/notifications/(?P<notification_id>[0-9]+)$',
        hypoderm.views.auth.views.Notifications.as_view(),
        name='notification_viewed'),

    # misc patient URLs
    url(r'^patient/home/$', hypoderm.views.patient.views.home, name='patient-home'),
    url(r'^patient/pharmacy_search/$', hypoderm.views.patient.views.PharmacySearchControllerView.as_view(), name='patient-pharmacy-search'),
    url(r'^patient/settings/$', hypoderm.views.patient.views.PatientProfileSettings.as_view(), name='patient-profile-settings'),

    # patient signup related URLs
    url(r'patient/signup/$', hypoderm.views.patient.signup.Parital.as_view(), name='patient-signup'),
    url(r'^patient/setup/$', hypoderm.views.patient.signup.Complete.as_view(), name='patient-setup'),

    # patient visit related URLs
    url(r'^patient/visit/(?P<visit_id>[0-9]+)/$', hypoderm.views.patient.visit.Index.as_view(),
        name='patient-visit'),
    url(r'^patient/visit/start$', hypoderm.views.patient.visit.Start.as_view(),
        name='patient-visit-start'),
    url(r'^patient/visit/condition$', hypoderm.views.patient.visit.Condition.as_view(),
        name='patient-visit-condition'),
    url(r'^patient/visit/photos$', hypoderm.views.patient.visit.Photos.as_view(),
        name='patient-visit-photos'),
    url(r'^patient/visit/review$', hypoderm.views.patient.visit.Review.as_view(),
        name='patient-visit-review'),
    url(r'^patient/visit/discount$', hypoderm.views.patient.visit.Discount.as_view(),
        name='patient-visit-discount'),
    url(r'^patient/visit/(?P<visit_id>[0-9]+)/status$',
        hypoderm.views.patient.visit.Status.as_view(), name='patient-visit-status'),
    url(r'^patient/visit/pharmacy', hypoderm.views.patient.visit.Pharmacy.as_view(),
        name='patient-visit-pharmacy'),

    # patient/provider visit common URLs
    url(r'^common/visit/(?P<visit_id>[0-9]+)/comments$',
        hypoderm.views.auth.visit.Comments.as_view(),
        name='auth-visit-comments'),

    # provider URLs
    url(r'^provider/dashboard/$', hypoderm.views.provider.views.dashboard,
        name='provider-dashboard'),
    url(r'^provider/signup/$', hypoderm.views.provider.signup.Partial.as_view(), name='provider-signup'),
    url(r'^provider/settings/$', hypoderm.views.provider.views.ProviderProfileSettings.as_view(), name='provider-profile-settings'),
    
    # provider fax referral URLs
    url(r'^provider/visit/(?P<visit_id>[0-9]+)/$',
        hypoderm.views.provider.visit.Index.as_view(), name='provider-visit'),
    url(r'^provider/visit/(?P<visit_id>[0-9]+)/faxreferral$',
        hypoderm.views.provider.visit.FaxReferral.as_view(),
        name='provider-visit-fax-referral'),

    # autocomplete URLs
    url(r'^autocomplete/diagnosis/$',
        hypoderm.views.provider.views.DiagnosisAutoComplete.as_view(), name='autocomplete-diagnosis'),

    # diagnoses URLs
    url(r'^diagnosis/(?P<visit_id>[0-9]+)/$',
        hypoderm.views.provider.visit.Diagnoses.as_view(), name='diagnoses'),

    # authentication URLs
    url(r'^login/$',
        'django.contrib.auth.views.login',
        name='login'),
    url(r'^logout/$',
        'django.contrib.auth.views.logout',
         {'next_page': '/login/'},
        name='logout'),
    url(r'^password_change/$',
        'django.contrib.auth.views.password_change',
        name='password_change'),
    url(r'^password_change/done/$',
        'django.contrib.auth.views.password_change_done',
        name='password_change_done'),
    url(r'^password_reset/$',
        'django.contrib.auth.views.password_reset',
        name='password_reset'),
    url(r'^password_reset/done/$',
        'django.contrib.auth.views.password_reset_done',
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        name='password_reset_confirm'),
    url(r'^reset/done/$',
        'django.contrib.auth.views.password_reset_complete',
        name='password_reset_complete'),

    url(r'common/photos/(?P<photo_id>[0-9]+)', hypoderm.views.auth.views.PhotoProxy.as_view(), name='photo-proxy'),    

]
