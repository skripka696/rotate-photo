from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in, user_logged_out, user_login_failed
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.dispatch import receiver, Signal
from django.template import Context, loader

from hypoderm.models import Notification

provider_created = Signal(providing_args=["user"])
patient_created = Signal(providing_args=["user"])
patient_visit_comment = Signal(providing_args=["visit_comment"])


@receiver(provider_created)
def handle_provider_created(sender, **kwargs):
    user = kwargs['user']

    context = Context({'user': user})

    msg_subject = loader.get_template('unauth/signup/provider/welcome_subject.txt')

    msg_plain = loader.get_template('unauth/signup/provider/welcome.txt')
    msg_html = loader.get_template('unauth/signup/provider/welcome.html')

    send_mail(
        msg_subject.render(context).strip(),
        msg_plain.render(context),
        'help@derm.healthcare',
        [user.email],
        html_message=msg_html.render(context),
    )

@receiver(patient_created)
def handle_patient_created(sender, **kwargs):
    user = kwargs['user']

    context = Context({'user': user})

    msg_subject = loader.get_template('unauth/signup/patient/welcome_subject.txt')

    msg_plain = loader.get_template('unauth/signup/patient/welcome.txt')
    msg_html = loader.get_template('unauth/signup/patient/welcome.html')

    send_mail(
        msg_subject.render(context).strip(),
        msg_plain.render(context),
        'help@derm.healthcare',
        [user.email],
        html_message=msg_html.render(context),
    )

@receiver(patient_visit_comment)
def handle_patient_visit_comment(sender, **kwargs):
    comment = kwargs['visit_comment']

    context = Context({'comment': comment})

    msg_subject = loader.get_template('auth/common/visit/comment_subject.txt')

    msg_plain = loader.get_template('auth/common/visit/comment.txt')
    msg_html = loader.get_template('auth/common/visit/comment.html')

    visit = comment.visit

    recipient_list = set()
    recipients = {comment.user for comment in visit.visitcomment_set.all()}
    for user in recipients:
        if comment.user.pk != user.pk:
            Notification(user=user,
                         message='New comment',
                         link=reverse('provider-visit',
                                      kwargs={'visit_id': visit.id})).save()

            recipient_list.add(user.patientprofile.email if hasattr(user, 'patientprofile') else user.providerprofile.email)

    send_mail(
        msg_subject.render(context).strip(),
        msg_plain.render(context),
        'help@derm.healthcare',
        list(recipient_list),
        html_message=msg_html.render(context)
    )
