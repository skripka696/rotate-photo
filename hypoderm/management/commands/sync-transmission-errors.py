import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.conf import settings

from hypoderm.lib.dosespot import get_transmission_errors, get_provider_page
from hypoderm.models import (
        ProviderProfile,
        Notification,
)

logger = logging.getLogger(__name__)

def get_active_providers():
    sessions = Session.objects.filter(expire_date__gte=timezone.now())
    uid_list = []

    for session in sessions:
        data = session.get_decoded()
        uid_list.append(data.get('_auth_user_id', None))

    users = User.objects.filter(id__in=uid_list)
    return ProviderProfile.objects.filter(user__in=users)

class Command(BaseCommand):
    def handle(self, *args, **options):
        resp = get_transmission_errors(settings.DOSESPOT_USER_ID)
        providers = get_active_providers()
        logger.debug(providers)
        
        for provider in providers:
            resp = get_transmission_errors(str(provider.clinician_id))
            
            if len(resp.RefillRequestsTransmissionErrors) > 0:
                user = provider.user
                if Notification.objects.filter(message='Transmission Errors',
                        user=user, is_active=True):
                    continue
                logger.info('Writing a transmission error notification')
                Notification(user=provider.user,
                        message='Transmission Errors',
                        link=get_provider_page()).save()
